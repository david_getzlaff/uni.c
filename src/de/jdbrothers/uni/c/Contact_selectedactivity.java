package de.jdbrothers.uni.c;

import java.io.ByteArrayInputStream;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.db.DBManager;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.nameparser;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;

public class Contact_selectedactivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_selected_item);
		((TextView) findViewById(R.id.contact_selected_telefon))
				.setText(datasave.contactselectedtelefon);
		((TextView) findViewById(R.id.contact_selected_address))
				.setText(datasave.contactselectedaddress);
		((TextView) findViewById(R.id.contact_selected_email))
				.setText(datasave.contactselectedemail);
		((TextView) findViewById(R.id.contact_selected_name))
				.setText(datasave.contactselectedname);

		getImageFromDB(datasave.contactselectedname);
		datasave.cleanPhoneNumber();
	}

	public void onCallClick(View v) {
		startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"
				+ datasave.contactselectedtelefon)));

	}

	public void onMailClick(View v) {
		nameparser p = new nameparser(datasave.contactselectedname);
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822"); // use from live device
		i.putExtra(Intent.EXTRA_EMAIL,
				new String[] { datasave.contactselectedemail });
		i.putExtra(Intent.EXTRA_TEXT, p.getMailHead());
		startActivity(Intent.createChooser(i, "Email"));

	}

	public void onNavigateClick(View v) {
		Intent i = new Intent(Intent.ACTION_VIEW,
				Uri.parse("google.navigation:q="
						+ datasave.contactselectedaddress));
		startActivity(i);
	}

	public void onInsertClick(View v) {
		Intent intent = new Intent(Intent.ACTION_INSERT);
		intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
		nameparser p = new nameparser(datasave.contactselectedname);

		intent.putExtra(ContactsContract.Intents.Insert.NAME,
				datasave.contactselectedname);
		intent.putExtra(ContactsContract.Intents.Insert.PHONE,
				datasave.contactselectedtelefon);
		intent.putExtra(ContactsContract.Intents.Insert.EMAIL_ISPRIMARY,
				datasave.contactselectedemail);
		intent.putExtra(ContactsContract.Intents.Insert.POSTAL_ISPRIMARY,
				datasave.contactselectedaddress);

		startActivity(intent);

	}

	public void getImageFromDB(String Name) {
		SQLiteDatabase db = new DBManager(this).getReadableDatabase();
		try {
			System.out.println("Name: " + Name);

			String getData = "Select "+DBDaten.T_Kontakt_S_Image+", " + DBDaten.T_Kontakt_S_Telefonnr
					+ ", " + DBDaten.T_Kontakt_S_Email + " From "
					+ DBDaten.T_Kontakt_Name + " Where "
					+ DBDaten.T_Kontakt_S_Name + " is '" + Name + "'";
//			String getBlobFromDB = "Select " + DatenbankDaten.T_Kontakt_S_Image
//					+ " From " + DatenbankDaten.T_Kontakt_Name + " Where "
//					+ DatenbankDaten.T_Kontakt_S_Name + " is '" + Name + "'";

			System.out.println("getData: " + getData);

			Cursor getImage = db.rawQuery(getData, null);
			getImage.moveToFirst();
			
			String[] Data= {"Blob", "EMail", "Telefon"};
			
			
			try {
				for (int i = 1; i < getImage.getColumnCount(); i++) {

					Data[i] = getImage.getString(i);
//					System.out.println(Data[i]);
				}
			} catch (Exception e) {
				Data[1] = "000";
				Data[2] = "";
			}
			
			ImageView iv = (ImageView)findViewById(R.id.quickContactBadge1);
			iv.setImageBitmap(BitmapFactory
					.decodeStream(new ByteArrayInputStream(getImage.getBlob(0))));
//

			Data= new String[3];

			getImage.close();

		} catch (Exception e) {
			try {
				((QuickContactBadge) findViewById(R.id.quickContactBadge1))
						.setImageResource(R.drawable.icon)
				;

			} catch (Exception e2) {
			}

		}
		db.close();
	}
}
