package de.jdbrothers.uni.c;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class appetizer_contact extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.appetizer_contact);

		TextView tv_aktVersion = (TextView) findViewById(R.id.tv_aktuelleVersion);
		PackageInfo pinfo = null;
		try {
			pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			tv_aktVersion.setText(tv_aktVersion.getText().toString() + " "
					+ pinfo.versionName);
		} catch (NameNotFoundException e) {
		}

	}

	public void onMailClick(View v) {
		Intent i = new Intent(Intent.ACTION_SEND);
		// i.setType("text/plain"); //use this line for testing in the emulator
		i.setType("message/rfc822"); // use from live device
		i.putExtra(Intent.EXTRA_EMAIL,
				new String[] { "appetizer.online@googlemail.com" });
		startActivity(Intent.createChooser(i, "Email"));

	}

	public void onWebClick(View v) {
		String url = "http://unic.zweipi.de";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}
}
