package de.jdbrothers.uni.c;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import de.jdbrothers.uni.c.hilfsklassen.PD;
import de.jdbrothers.uni.c.hilfsklassen.newMenu;

public class feedbackActivity extends Activity {
public static String URL;
public static String[] keys;
public static String[] data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);
		setTitle(newMenu.feedback);

	}

	public void onAbortClick(View v) {
		finish();
	}

	public void onSendClick(View v) {
		keys =new String[] { "pw", "studiengang","email","bewertung","feedbacktype","feedbacktext"};

		URL = "http://julian.dorn.tk/ba/unic.php";
		String pw = "ph2swUm3";

		String studiengang = ((EditText) findViewById(R.id.et_feedback_studiengang))
				.getText().toString();
		String email = ((EditText) findViewById(R.id.et_feedback_email))
				.getText().toString();
		String feedbacktext = ((EditText) findViewById(R.id.et_feedback_feedbacktext))
				.getText().toString();
		String category = ((Spinner) findViewById(R.id.sp_feedback_feedbackspin))
				.getSelectedItem().toString();
		int wertung = (int) ((RatingBar) findViewById(R.id.rb_feedback_ratingbar))
				.getRating();

		
		System.out.println("-------------Feedback---------------\n"
				+"SG: 			"+studiengang
				+"Mail: 		"+email
				+"Wertung: 		"+wertung
				+"Kategorie: 	"+category
				+"Feedbacktext: "+feedbacktext);
		
		
		data =new String[] { pw, studiengang, email, ""+wertung, category, feedbacktext};
		
		PD.runIT(this, "Lade Bewertung hoch");
		

}}
