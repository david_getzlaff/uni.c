package de.jdbrothers.uni.c.db;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import de.jdbrothers.uni.c.hilfsklassen.Checker;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.dateWorker;

public class DBAPI extends Activity {

	public static String ViewDurchschnitt(Context c) {

		SQLiteDatabase db = new DBManager(c).getReadableDatabase();
		Cursor cur = db.rawQuery("Select * from durchschnitt", null);
		
		cur.moveToFirst();

		String ret = cur.getString(0);

		cur.close();
		db.close();

		return ret;
	}

	public static String[] getHeaders(Context c, String table) {

		SQLiteDatabase db = new DBManager(c).getReadableDatabase();
		Cursor cur = db.rawQuery("Select * from " + table, null);
		String[] headers = cur.getColumnNames();
		cur.close();
		db.close();
		return headers;
	}

	public static String[][] getTables(Context c) {

		SQLiteDatabase db = new DBManager(c).getReadableDatabase();
		Cursor cur = db
				.rawQuery(
						"Select name from SQLITE_Master where type = 'table' order by name",
						null);

		String[][] ret = new String[cur.getCount()][cur.getColumnCount()];
		int zeile = 0;
		while (cur.moveToNext()) {
			for (int i = 0; i < cur.getColumnCount(); i++) {
				ret[zeile][i] = cur.getString(i);
			}
			zeile++;
		}
		cur.close();
		db.close();

		return ret;
	}

	public static int countRows(Context c, String table) {
		return countRows(c, table, null);
	}

	public static int countRows(Context c, String table, String where) {
		String query = "Select Count (*) From " + table + checkWhere(where);
		System.out.println("CountRows-Query: " + query);
		SQLiteDatabase db = new DBManager(c).getReadableDatabase();
		Cursor cur = db.rawQuery(query, null);
		cur.moveToNext();
		int i = cur.getInt(0);
		// System.out.println(query + " z�hlt  " + i+" rows");
		cur.close();
		db.close();
		return i;
	}

	private static String checkWhere(String where) {
		return ((where != null && where != "") ? (where.contains("where") ? " "
				+ where + " " : " WHERE " + where) : "");
	}

	public static String[][] getData(Context c, String table, String selection,
			String where) {
		String[][] returnstring = { { "" } };
		String[] tmp = { selection };
		String[][] checkstring = getData(c, table, tmp, where);
		if (Checker.Check(checkstring)) {
			return checkstring;
		} else {
			return returnstring;
		}
	}

	public static String[][] getData(Context c, String table,
			String[] selection, String where) {
		String query = "Select ";
		for (int i = 0; i < selection.length; i++) {
			query = query + selection[i].toString() + ", ";
		}
		query = query.substring(0, query.length() - 2) + " From " + table
				+ checkWhere(where);

		SQLiteDatabase db = new DBManager(c).getReadableDatabase();
		Cursor getIT = db.rawQuery(query, null);
		String[][] tmp = new String[getIT.getCount()][getIT.getColumnCount()];
		int zeile = 0;
		while (getIT.moveToNext()) {
			for (int i = 0; i < getIT.getColumnCount(); i++) {
				tmp[zeile][i] = getIT.getString(i);
			}
			zeile++;
		}
		getIT.close();
		db.close();
		return tmp;
	}

	public static String createInsertQuery(String Table, String[] ColumnName,
			String[] Werte) {
		String Query = "insert into " + Table + " (";

		for (int i = 0; i < ColumnName.length; i++) {
			Query = Query + ColumnName[i].toString() + ", ";
		}
		Query = Query.substring(0, Query.length() - 2) + ") Values (";

		for (int i = 0; i < Werte.length; i++) {
			Query = Query + "'" + Werte[i].toString() + "', ";
		}
		Query = Query.substring(0, Query.length() - 2) + ")";
		return Query;
	}

	public static String[] createInsertQuery(String table, String[] ColumnName,
			String[][] Werte) {

		String[] ret = new String[Werte.length];

		for (int len = 0; len < Werte.length; len++) {
			String Query = "insert into " + table + " (";

			for (int i = 0; i < ColumnName.length; i++) {
				Query = Query + ColumnName[i].toString() + ", ";
			}
			Query = Query.substring(0, Query.length() - 2) + ") Values (";

			for (int i = 0; i < Werte[0].length; i++) {
				Query = Query + "'" + Werte[len][i] + "', ";
			}
			Query = Query.substring(0, Query.length() - 2) + ")";
			ret[len] = Query;
			// System.out.println(Query);
		}
		return ret;
	}

	public static int getFilterpos(Context c) {
		return Integer.parseInt(getData(c, DBDaten.T_Settings_Name,
				DBDaten.T_Settings_S_Wert, " where "
						+ DBDaten.T_Settings_S_Attribut + " = '"
						+ DBDaten.Settings_Filterposition + "'")[0][0]);
	}

	public static void setFilterpos(Context c, int pos) {
		if (countRows(c, DBDaten.T_Settings_Name, " where "
				+ DBDaten.T_Settings_S_Attribut + " = 'Filterposition'") > 0)
			// l�schen und speichern
			delete(c, DBDaten.T_Settings_Name, " where "
					+ DBDaten.T_Settings_S_Attribut + " = 'Filterposition'");
		insertIntoSettings(c, new String[] { DBDaten.Settings_Filterposition,
				"" + pos });
	}

	public static void insertIntoSettings(Context c, String[] data) {
		insertInto(c, DBDaten.T_Settings_Name, new String[][] {
				{ DBDaten.T_Settings_S_Attribut, DBDaten.T_Settings_S_Wert },
				data });
	}

	public static void insertInto(Context c, String table, String[][] data) {
		String[] headers = data[0];
		String insert = "";
		SQLiteDatabase db = new DBManager(c).getReadableDatabase();

		for (int i = 1; i < data.length; i++) {
			insert = createInsertQuery(table, headers, data[i]);
			try {
				db.execSQL(insert);
			} catch (Exception e) {
				System.out.println("-----------insert into-----------");
				e.printStackTrace();
				System.out.println("--------insert into ende---------");
			}
			insert = "";
		}
		db.close();
	}

	public static void insertInto(Context c, String table, String[] headers,
			String[][] data) {
		String[] tmp = createInsertQuery(table, headers, data);

		SQLiteDatabase db = new DBManager(c).getReadableDatabase();
		try {
			for (int i = 0; i < tmp.length; i++) {
				// System.out.println(tmp[i]);
				db.execSQL(tmp[i]);
			}
			System.out.println("Werte eingef�gt");
		} catch (Exception e) {
			System.out.println("Fehler bei InsertInto");
			e.printStackTrace();
		}
		db.close();
	}

	public static void setLoginData(Context c, String[] MatrNr_PW_TO_TOPos) {
		String[] tmp = { DBDaten.Settings_MatrNr, DBDaten.Settings_PW,
				DBDaten.Settings_TimeOut, DBDaten.Settings_TimeOutPos };

		for (int i = 0; i < tmp.length; i++) {
			if (countRows(c, DBDaten.T_Settings_Name, " where "
					+ DBDaten.T_Settings_S_Attribut + " = '" + tmp[i] + "'") > 0) {
				delete(c, DBDaten.T_Settings_Name, " where "
						+ DBDaten.T_Settings_S_Attribut + " = '" + tmp[i] + "'");
			}
			insertIntoSettings(c,
					new String[] { tmp[i], MatrNr_PW_TO_TOPos[i] });
		}

		datasave.refresh(c);
		((Activity) c).finish();
	}

	public static String[] getLoginData(Context c) {
		String[] ret = { DBDaten.Settings_MatrNr, DBDaten.Settings_PW,
				DBDaten.Settings_TimeOut, DBDaten.Settings_TimeOutPos };
		for (int i = 0; i < ret.length; i++) {
			ret[i] = getData(c, DBDaten.T_Settings_Name,
					DBDaten.T_Settings_S_Wert, DBDaten.T_Settings_S_Attribut
							+ " like '%" + ret[i] + "%'")[0][0];
			if (ret[i].length() == 0) {
				switch (i) {
				case 0:
					ret[i] = "000";
					break;
				case 1:
					ret[i] = "";
					break;
				case 3:
					ret[i] = "5";
					break;
				}
			}
		}
		return ret;
	}

	public static void updateLoginData(Context c, String[] newData) {
		String[] tmp = { DBDaten.Settings_MatrNr, DBDaten.Settings_PW,
				DBDaten.Settings_TimeOut, DBDaten.Settings_TimeOutPos };
		for (int i = 0; i < tmp.length; i++)
			delete(c, DBDaten.T_Settings_Name, " where "
					+ DBDaten.T_Settings_S_Attribut + " is '" + tmp[i] + "'");
		setLoginData(c, newData);
	}

	public static void Update(Context c, String Tabelle, String where,
			String[][] newData) {
		delete(c, Tabelle, where);
		insertInto(c, Tabelle, newData);
	}

	public static void delete(Context c, String table) {
		delete(c, table, "");
	}

	public static void delete(Context c, String table, String where) {
		SQLiteDatabase db = new DBManager(c).getReadableDatabase();
		try {
			db.execSQL("Delete From " + table + checkWhere(where));
		} catch (Exception e) {
			T.e("DBAPI", e);
		}
		db.close();
	}

	public static boolean Check4Entries(Context c) {
		int matrnr = countRows(c, DBDaten.T_Settings_Name, " where "
				+ DBDaten.T_Settings_S_Attribut + " like '%"
				+ DBDaten.Settings_MatrNr + "%'");
		int pw = countRows(c, DBDaten.T_Settings_Name, " where "
				+ DBDaten.T_Settings_S_Attribut + " like '%"
				+ DBDaten.Settings_PW + "%'");
		if (matrnr == 0 && pw == 0)// Keine Logindaten vorhanden !
			return true;
		else
			return false;
	}

	public static void aktualisiereUpdateTable(Context c, String was) {
		aktualisiereUpdateTable(c, was, dateWorker.getCurrentTime());
	}

	public static void aktualisiereUpdateTable(Context c, String was,
			String wann) {
		delete(c, DBDaten.T_lastUpdate_Name, "where was = '" + was + "'");
		insertInto(c, DBDaten.T_lastUpdate_Name, new String[][] {
				{ DBDaten.T_lastUpdate_S_Was, DBDaten.T_lastUpdate_S_Wann },
				{ was, wann } });
	}
}