package de.jdbrothers.uni.c.db;

import android.database.sqlite.SQLiteDatabase;

public class DBDaten {

	// Standart
	public static String DB_Name = "Datenbank.db";
	public static int DB_Version = 1;

	// ===============================================================================
	// ===============================================================================

	// Strings f�r Noten
	public static String T_Noten_Name = "Noten";
	public static String T_Noten_S_Semester = "Semester";
	public static String T_Noten_S_Modul = "Modul";
	public static String T_Noten_S_Note = "Note";
	public static String T_Noten_S_Gewichtung = "Gewichtung";

	public static String CreateTableNoten = "Create Table if not Exists "
			+ T_Noten_Name + " (" + T_Noten_S_Semester + " TEXT, "
			+ T_Noten_S_Modul + " TEXT, " + T_Noten_S_Note + " TEXT, "
			+ T_Noten_S_Gewichtung + " text default 3)";

	public static String DropTableNoten = "Drop Table if exists "
			+ T_Noten_Name;

	public static String getNoten_RAW = "Select " + T_Noten_S_Semester + ", "
			+ T_Noten_S_Modul + ", " + T_Noten_S_Note + " From " + T_Noten_Name
			+ " Where " + T_Noten_S_Note + " Not NULL";

	// ===============================================================================
	// Noten+Verteilung

	private static String notenUndGewichtung = "NotenUndGewichtung";

	private static String CreateNotenUndGewichtung = "CREATE TABLE if not exists "
			+ notenUndGewichtung
			+ " (" + T_Noten_S_Semester + " TEXT, "
			+ T_Noten_S_Modul + " TEXT, " + T_Noten_S_Note + " TEXT, "
			+ T_Noten_S_Gewichtung + " text default 3)";

	private static String DropNotenUndGewichtung = "Drop Table if exists "
			+ notenUndGewichtung;

	private static String �bernimmnNoteninTMP = "insert into "
			+ notenUndGewichtung
			+ " (Semester, Modul, Note ) select Semester, Modul, Note from "
			+ T_Noten_Name;

	private static String �bernimmnNotenInNeueNoten = "insert into "
			+ T_Noten_Name + " select * from " + notenUndGewichtung;

	private static String ViewDurchschnitt = "CREATE VIEW if not exists durchschnitt AS "
			+ "select round(sum(note*gewichtung)/sum(gewichtung),2) "
			+ "from "
			+ T_Noten_Name + " " + "where note not like 'null'";

	public static void updateNoten(SQLiteDatabase db) {
		// tmp tabelle erstellen
		// noten --> tmp
		// alte noten-tabelle l�schen
		// neue noten-tabelle erstellen
		// tmp --> noten kopieren
		// tmp l�schen
		// view f�r � erstellen

		// tmp erstellen
		db.execSQL(DropNotenUndGewichtung);
		db.execSQL(CreateNotenUndGewichtung);

		// noten --> tmp
		db.execSQL(DBDaten.�bernimmnNoteninTMP);

		// alte noten-tabelle l�schen
		db.execSQL(DropTableNoten);

		// neue noten-tabelle erstellen
		db.execSQL(CreateTableNoten);

		// tmp --> noten kopieren
		db.execSQL(DBDaten.�bernimmnNotenInNeueNoten);

		// tmp l�schen
		db.execSQL(DropNotenUndGewichtung);

		// view f�r � erstellen
		 db.execSQL(DBDaten.ViewDurchschnitt);

	}

	// ===============================================================================
	// public static String T_Verteilung_Name = "Verteilung";
	// public static String T_Verteilung_S_Semester = "Semester";
	// public static String T_Verteilung_S_Modul = "Modul";
	// public static String T_Verteilung_S_Notenschl�ssel = "Notenschl�ssel";
	//
	// public static String CreateTableVerteilung =
	// "Create Table IF Not Exists "
	// + T_Verteilung_Name + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
	// + T_Verteilung_S_Semester + " TEXT, " + T_Verteilung_S_Modul
	// + " TEXT, " + T_Verteilung_S_Notenschl�ssel + " TEXT DEFAULT 3)";
	//
	// public static String DropTableVerteilung = "Drop Table if exists "
	// + T_Verteilung_Name;
	//
	// public static String getVerteilung_RAW = "Select "
	// + T_Verteilung_S_Semester + ", " + T_Verteilung_S_Modul + ", "
	// + T_Verteilung_S_Notenschl�ssel + " From " + T_Verteilung_Name;

	// ===============================================================================

	// Strings f�r News
	public static String T_News_Name = "News";
	public static String T_News_S_Datum = "Datum";
	public static String T_News_S_Absender = "Absender";
	public static String T_News_S_Nachricht = "Nachricht";

	public static String CreateTableNews = "Create Table if not exists "
			+ T_News_Name + " (" + T_News_S_Datum + " TEXT, "
			+ T_News_S_Absender + " TEXT, " + T_News_S_Nachricht + " TEXT)";

	public static String DropTableNews = "Drop Table if exists " + T_News_Name;;

	public static String getNews_RAW = "Select " + T_News_S_Datum + ", "
			+ T_News_S_Absender + ", " + T_News_S_Nachricht + " From "
			+ T_News_Name;

	// ===============================================================================
	// ===============================================================================

	// Strings f�r Ansprechpartner
	public static String T_Kontakt_Name = "Ansprechpartner";
	public static String T_Kontakt_S_Name = "Name";
	public static String T_Kontakt_S_Titel = "Titel";
	public static String T_Kontakt_S_Telefonnr = "Phone";
	public static String T_Kontakt_S_Email = "Email";
	public static String T_Kontakt_S_Adresse = "Adress";
	public static String T_Kontakt_S_Image = "Image";

	public static String CreateTableKontakte = "Create Table if not exists "
			+ T_Kontakt_Name + " (" + T_Kontakt_S_Name + " TEXT, "
			+ T_Kontakt_S_Titel + " TEXT, " + T_Kontakt_S_Telefonnr + " TEXT, "
			+ T_Kontakt_S_Email + " TEXT, " + T_Kontakt_S_Adresse + " TEXT, "
			+ T_Kontakt_S_Image + " blob)";

	public static String DropTableKontakte = "Drop Table if exists "
			+ T_Kontakt_Name;

	public static String getContacts_RAW = "Select " + T_Kontakt_S_Name + ", "
			+ T_Kontakt_S_Titel + ", " + T_Kontakt_S_Telefonnr + ", "
			+ T_Kontakt_S_Email + ", " + T_Kontakt_S_Adresse + ", "
			+ T_Kontakt_S_Image + " From " + T_Kontakt_Name;

	// ===============================================================================
	// ===============================================================================

	public static String T_Plan_Name = "Stundenplan";
	public static String T_Plan_S_KW = "KW";
	public static String T_Plan_S_Datum = "Datum";
	public static String T_Plan_S_Von = "Von";
	public static String T_Plan_S_Bis = "Bis";
	public static String T_Plan_S_Montag = "Montag";
	public static String T_Plan_S_Dienstag = "Dienstag";
	public static String T_Plan_S_Mittwoch = "Mittwoch";
	public static String T_Plan_S_Donnerstag = "Donnerstag";
	public static String T_Plan_S_Freitag = "Freitag";
	public static String T_Plan_S_Samstag = "Samstag";

	public static String CreateTableStundenplan = "CREATE TABLE if not exists "
			+ T_Plan_Name + " (" + T_Plan_S_KW + " TEXT, " + T_Plan_S_Datum
			+ " TEXT, " + T_Plan_S_Von + " TEXT, " + T_Plan_S_Bis + " TEXT, "
			+ T_Plan_S_Montag + " TEXT, " + T_Plan_S_Dienstag + " TEXT, "
			+ T_Plan_S_Mittwoch + " TEXT, " + T_Plan_S_Donnerstag + " TEXT, "
			+ T_Plan_S_Freitag + " TEXT, " + T_Plan_S_Samstag + " TEXT)";

	public static String DropTableStundenplan = "Drop TABLE if exists "
			+ T_Plan_Name;

	// ===============================================================================
	// ===============================================================================

	public static String T_Angebot_Name = "Mensa_Angebot";
	public static String T_Abgebot_S_Mid = "Mid";
	public static String T_Abgebot_S_Datum = "Datum";
	public static String T_Abgebot_S_title = "title";
	public static String T_Abgebot_S_description = "description";
	public static String T_Abgebot_S_price = "price";
	public static String T_Abgebot_S_URL = "URL";

	public static String CreateTableAngebot_morgen = "Create Table if not exists "
			+ T_Angebot_Name
			+ " ("
			+ T_Abgebot_S_Mid
			+ " Text, "
			+ T_Abgebot_S_Datum
			+ " TEXT, "
			+ T_Abgebot_S_title
			+ " TEXT, "
			+ T_Abgebot_S_description
			+ " TEXT, "
			+ T_Abgebot_S_price
			+ " TEXT, " + T_Abgebot_S_URL + " TEXT)";

	public static String DropTableAngebot = "Drop Table if exists "
			+ T_Angebot_Name;

	// ===============================================================================
	// ===============================================================================

	public static String T_Mensen_Name = "Mensen";
	public static String T_Mensen_S__id = "_id";
	public static String T_Mensen_S_Name = "Name";
	public static String T_Mensen_S_Anschrift = "Anschrift";
	public static String T_Mensen_S_GPS = "GPS";
	public static String T_Mensen_S_wayne1 = "wayne1";
	public static String T_Mensen_S_wayne2 = "wayne2";
	public static String T_Mensen_S_wayne3 = "wayne3";

	public static String CreateTableMensen = "Create Table if not exists "
			+ T_Mensen_Name + " (" + T_Mensen_S__id
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + T_Mensen_S_Name
			+ " TEXT, " + T_Mensen_S_Anschrift + " TEXT, " + T_Mensen_S_GPS
			+ " TEXT, " + T_Mensen_S_wayne1 + " TEXT, " + T_Mensen_S_wayne2
			+ " TEXT, " + T_Mensen_S_wayne3 + " TEXT)";

	public static String DropTableMensen = "Drop Table if exists "
			+ T_Angebot_Name;

	// ===============================================================================
	// ===============================================================================

	public static String T_Settings_Name = "Settings";
	public static String T_Settings_S_Attribut = "Attribut";
	public static String T_Settings_S_Wert = "Wert";
	public static String T_Settings_S_Beschreibung = "Beschreibung";

	public static String CreateTableSettings = "Create Table if not exists "
			+ T_Settings_Name + " (" + T_Settings_S_Attribut
			+ " TEXT PRIMARY KEY, " + T_Settings_S_Wert + " TEXT, "
			+ T_Settings_S_Beschreibung + " TEXT)";

	public static String DropTableSettings = "Drop Table if exists "
			+ T_Settings_Name;

	public static String Settings_Filterposition = "Filterposition";
	public static String Settings_MatrNr = "MatrNr";
	public static String Settings_PW = "PW";
	public static String Settings_TimeOut = "TimeOut";
	public static String Settings_TimeOutPos = "TimeOutPos";

	// ===============================================================================
	// ===============================================================================

	public static String T_lastUpdate_Name = "LastUpdate";
	public static String T_lastUpdate_S_Was = "Was";
	public static String T_lastUpdate_S_Wann = "Wann";

	public static String CreateTableLastUpdate = "Create Table if not exists "
			+ T_lastUpdate_Name + " (" + T_lastUpdate_S_Was
			+ " TEXT PRIMARY KEY, " + T_lastUpdate_S_Wann + " TEXT)";

	public static String DropTableLastUpdate = "Drop Table if exists "
			+ T_lastUpdate_Name;
}
