package de.jdbrothers.uni.c.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBManager extends SQLiteOpenHelper {

	public static String Erstinitialisierung = "Erstinitialisierung";
	public static String updateNoten= "updateNoten";

	public DBManager(Context c) {
		super(c, DBDaten.DB_Name, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		onUpgrade(db, Erstinitialisierung);
	}

	public void onUpgrade(SQLiteDatabase db, String ToChange) {
		if (ToChange.equals(Erstinitialisierung)) {
			db.execSQL(DBDaten.CreateTableSettings);
			db.execSQL(DBDaten.CreateTableNoten);
			db.execSQL(DBDaten.CreateTableNews);
			db.execSQL(DBDaten.CreateTableKontakte);
			db.execSQL(DBDaten.CreateTableStundenplan);
			db.execSQL(DBDaten.CreateTableAngebot_morgen);
			db.execSQL(DBDaten.CreateTableMensen);
			db.execSQL(DBDaten.CreateTableLastUpdate);
		}

		if (ToChange.equals(updateNoten)) {
//			db.execSQL(DBDaten.DropNotenUndGewichtung);
//			db.execSQL(DBDaten.CreateNotenUndGewichtung);
//			
//			db.execSQL(DBDaten.übernimmnNoten);
//			db.execSQL(DBDaten.ViewDurchschnitt);
//			
			DBDaten.updateNoten(db);
			
		}

		if (ToChange.contains(DBDaten.T_Settings_Name)) {
			db.execSQL(DBDaten.DropTableSettings);
			db.execSQL(DBDaten.CreateTableSettings);
		}

		if (ToChange.equals(DBDaten.T_Noten_Name)) {
			db.execSQL(DBDaten.DropTableNoten);
			db.execSQL(DBDaten.CreateTableNoten);
		}

		if (ToChange.equals(DBDaten.T_News_Name)) {
			db.execSQL(DBDaten.DropTableNews);
			db.execSQL(DBDaten.CreateTableNews);
		}
		if (ToChange.contains("Kontakte") || ToChange.contains("Ansprech")
				|| ToChange.equals(DBDaten.T_Kontakt_Name)) {
			db.execSQL(DBDaten.DropTableKontakte);
			db.execSQL(DBDaten.CreateTableKontakte);
		}

		if (ToChange.contains(DBDaten.T_Plan_Name)) {
			db.execSQL(DBDaten.DropTableStundenplan);
			db.execSQL(DBDaten.CreateTableStundenplan);
		}

		if (ToChange.contains(DBDaten.T_Angebot_Name)) {
			db.execSQL(DBDaten.DropTableAngebot);
			db.execSQL(DBDaten.CreateTableAngebot_morgen);
		}
		if (ToChange.contains(DBDaten.T_Mensen_Name)) {
			db.execSQL(DBDaten.DropTableMensen);
			db.execSQL(DBDaten.CreateTableMensen);
		}

		if (ToChange.contains(DBDaten.T_lastUpdate_Name)) {
			db.execSQL(DBDaten.DropTableLastUpdate);
			db.execSQL(DBDaten.CreateTableLastUpdate);
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
	}

}
