package de.jdbrothers.uni.c.db;

import java.io.File;
import java.util.ArrayList;

public class s {

	private static void sy(final String s) {
		System.out.println(s);
	}

	public s(String text) {
		if (text != null)
			System.out.println(text);
		else
			s.sy("‹bergebener Wert ist NULL");
	}

	public s(String[] text) {
		if (text != null)
			for (int i = 0; i < text.length; i++)
				sy(text[i]);
		else
			sy("‹bergebener Wert ist NULL");
	}

	public s(String[][] text) {
		for (int i = 0; i < text.length; i++)
			for (int j = 0; j < text[0].length; j++)
				sy(text[i][j]);
	}

	public s(ArrayList<String> text) {
		for (int i = 0; i < text.size(); i++)
			sy(text.get(i));
	}

	public s(int zahl) {
		sy("" + zahl);
	}

	public s(int zahl1, int zahl2) {
		sy("Zahl 1: " + zahl1);
		sy("Zahl 2: " + zahl2);
	}

	public s(long l) {
		s.sy("" + l);
	}

	public s(String wobei, Exception e) {
		s.sy("----- Fehler bei " + wobei + " ------");
		e.printStackTrace();
		s.sy("------- " + wobei + " ------");

	}

	public s(File f) {
		s.sy(f.toString());
	}

	public s(boolean b) {
		s.sy(b + "");
	}

	public s(double[] d) {
		String ret = "{";
		for (int i = 0; i < d.length; i++)
			ret = ret + d[i] + ((i < d.length - 1) ? ", " : "}");
		s.sy(ret);
	}

	public s(File[] f) {
		if (f.length < 0)
			for (int i = 0; i < f.length; i++)
				s.sy(f[i].getPath().toString());
	}
}
