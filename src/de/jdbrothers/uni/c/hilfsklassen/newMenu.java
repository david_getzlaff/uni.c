package de.jdbrothers.uni.c.hilfsklassen;

import android.R;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import de.jdbrothers.uni.c.PlanActivity;
import de.jdbrothers.uni.c.ResetMenueActivity;
import de.jdbrothers.uni.c.NotenschluesselActivity;
import de.jdbrothers.uni.c._mainActivity;
import de.jdbrothers.uni.c.appetizer_contact;
import de.jdbrothers.uni.c.feedbackActivity;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;

public class newMenu extends Activity {

	private static int[] menuPos;
	private static String[] menuText;

	// =========================================================================================
	// Davids statische MenuClick Sachen
	// =========================================================================================
	public static String toDo = "Wird demnächst noch implementiert";
	public static String neuLaden = "Neu laden";
	public static String filter = "Filter";

	public static String filterNachSemester = "Filtere nach Semester";
	public static String filterNachAbsender = "Filtere nach Absender";

	public static String notenschlüssel = "Notenschlüssel";
	public static String resetmenü = "Reset-Menü";
	public static String hauptmenü = "Hauptmenü";
	public static String feedback = "Feedback";
	public static String aboutUs = "About us";
	public static String beenden = "Beenden";
	public static String planFürWocheAktualiesieren = "Woche aktualisieren";
	public static String allesLöschen = "Alle Einträge löschen";

	// Titel für die einzelnen Activities, die ein MENÜ bekommen !
	// Das A am Ende steht für eine Activity !
	public static String mainA = "_mainActivity";
	public static String loginA = "LoginActivity";
	public static String notenA = "NotenActivity";
	public static String newsA = "NewsActivity";
	public static String contactA = "ContactActivity";
	public static String planA = "PlanActivity";
	public static String resetA = "ResetActivity";
	public static String mensaA = "MensaActivity";

	public static Menu createMenu(Menu menu, String[] Strings) {
		menuPos = new int[Strings.length + 3];
		menuText = new String[Strings.length + 3];

		menuText[menuPos.length - 3] = feedback;
		menuText[menuPos.length - 2] = aboutUs;
		menuText[menuPos.length - 1] = beenden;

		int i = 0;
		for (int pos = 0; pos < menuPos.length; pos++) {
			menuPos[pos] = Menu.FIRST + pos;
		}

		for (i = 0; i < Strings.length; i++) {
			if (!Strings[i].equals("")) {
				boolean hasIcon = false;
				menuText[i] = Strings[i];

				// Definieren, welches Item ein Icon bekommen soll
				if (Strings[i].contains(neuLaden) || Strings[i].contains(planFürWocheAktualiesieren)) {
					menu.add(0, menuPos[i], i, menuText[i]).setIcon(
							de.jdbrothers.uni.c.R.drawable.ic_menu_refresh);
					hasIcon = true;
				}
				if (Strings[i].contains(notenschlüssel)) {
					menu.add(0, menuPos[i], i, menuText[i]).setIcon(
							R.drawable.ic_menu_agenda);
					hasIcon = true;
				}
				if (Strings[i].contains(filter)) {
					menu.add(0, menuPos[i], i, menuText[i]).setIcon(
							de.jdbrothers.uni.c.R.drawable.filter_48x48);
					hasIcon = true;
				}
				if (Strings[i].contains(resetmenü)) {
					menu.add(0, menuPos[i], i, menuText[i]).setIcon(
							R.drawable.ic_menu_delete);
					hasIcon = true;
				}
				if (Strings[i].contains(hauptmenü)) {
					menu.add(0, menuPos[i], i, menuText[i]).setIcon(
							de.jdbrothers.uni.c.R.drawable.ic_menu_home);
					hasIcon = true;
				}

				if (Strings[i].contains(allesLöschen)) {
					menu.add(0, menuPos[i], i, menuText[i]).setIcon(
							R.drawable.ic_menu_delete);
					hasIcon = true;
				}

				if (hasIcon == false) {
					menu.add(0, menuPos[i], i, menuText[i]);
				}
			}
		}
		menu.add(1, menuPos[i], menuPos[i], menuText[i]).setIcon(
				R.drawable.ic_menu_send);
		menu.add(1, menuPos[i + 1], menuPos[i + 1], menuText[i + 1]).setIcon(
				R.drawable.ic_menu_info_details);
		menu.add(1, menuPos[i + 2], menuPos[i + 2], menuText[i + 2]).setIcon(
				R.drawable.ic_menu_close_clear_cancel);
		return menu;
	}

	public static MenuItem getAction(MenuItem item, Activity a) {

		// Filter
		if (item.getTitle().equals(filter)) {

			if (a.getTitle().equals(notenA)) {
				AD.filter(a, filterNachSemester, DBDaten.T_Noten_Name,
						DBDaten.T_Noten_S_Semester);
			}
			if (a.getTitle().equals(newsA)) {
				AD.filter(a, filterNachAbsender, DBDaten.T_News_Name,
						DBDaten.T_News_S_Absender);
			}

		}

		// Neu laden
		if (item.getTitle().equals(neuLaden)) {
			if (a.getTitle().equals(notenA)) {
				PD.runIT(a, "Aktualisiere Noten");
			}
			if (a.getTitle().equals(newsA)) {
				PD.runIT(a, "Aktualisiere News");
			}
			if (a.getTitle().equals(contactA)) {
				PD.runIT(a, "Aktualisiere Kontakte");
			}
			if (a.getTitle().equals(mensaA)) {
				PD.runIT(a, "Aktualisiere Essen");
			}
		}
		
		if (item.getTitle().equals(planFürWocheAktualiesieren) && a.getTitle().equals(planA)) {
			DBAPI.delete(a.getApplicationContext(), DBDaten.T_Plan_Name, "where Datum = '"+PlanActivity.sp.getSelectedItem().toString()+"'");
			PD.runIT(a, "Aktualisiere Stundenplan für gewählte Woche");
		}

		// Selten
		if (item.getTitle().equals(hauptmenü)) {
			a.finish();
		}

		if (item.getTitle().equals(resetmenü)) {
			a.startActivity(new Intent(a, ResetMenueActivity.class));
		}

		if (item.getTitle().equals(allesLöschen)) {
			DBAPI.delete(a.getApplicationContext(), DBDaten.T_Plan_Name);
		}

		// statisches Zeug
		if (item.getTitle().equals(feedback)) {
			a.startActivity(new Intent(a, feedbackActivity.class));
		}

		if (item.getTitle().equals(aboutUs)) {
			a.startActivity(new Intent(a, appetizer_contact.class));
		}

		if (item.getTitle().equals(beenden)) {
			beenden(a);
		}
		
		if (item.getTitle().equals(notenschlüssel)) {
//			T.t(a.getApplicationContext(), "Wird noch implementiert !");
			a.startActivity(new Intent(a, NotenschluesselActivity.class));
		}
		
		return item;
	}

	private static void beenden(Activity a) {
		if ((a.getTitle()).equals(mainA)) {
			// T.s("newMenu", "in " + mainA + ", System.exit(0) aufgerufen");
			System.exit(0);
			_mainActivity.übergabe = false;
		} else {
			// T.s("newMenu", "Nicht in " + mainA + ", finish gesendet");
			_mainActivity.übergabe = true;
			a.finish();
		}
	}

}
