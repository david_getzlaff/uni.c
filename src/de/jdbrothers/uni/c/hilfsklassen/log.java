package de.jdbrothers.uni.c.hilfsklassen;

import android.app.Activity;
import android.util.Log;

public class log {

	public log(Activity activity, String meldung) {
		Log.i(activity.getTitle().toString(), meldung);
	}

	public log(String activity, String meldung) {
		Log.i(activity, meldung);
	}

	public log(String activity, String[] meldungen) {
		for (int i = 0; i < meldungen.length; i++)
			Log.i(activity, meldungen[i]);
	}
	
	public log(Activity activity, String[] meldungen) {
			new log(activity.getTitle().toString(), meldungen);
	}
	

}
