package de.jdbrothers.uni.c.hilfsklassen;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import android.widget.Spinner;

public class dateWorker {

	static GregorianCalendar cal = new GregorianCalendar(
			TimeZone.getTimeZone("GMT"), Locale.GERMANY);
	static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

	public static String getPreviousWeek(int weekAgo) {
		return buildWeekString(-weekAgo);
	}
	
	public static String getCurrentTime(){
		return new SimpleDateFormat ("dd.MM.yyyy - HH:mm:ss").format(new Date());
	}

	public static String getDateOfDay(int TagVonTabhost, Spinner s) {
		cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.GERMANY);
		String anfangsDatum = s.getSelectedItem().toString().split(" ")[0];
		Date d = new Date();
		try {
			d = sdf.parse(anfangsDatum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// d ist nun auf Montag gesetzt
		cal.setTime(d);

		cal.add(Calendar.DAY_OF_WEEK, TagVonTabhost);

		return sdf.format(cal.getTime());
	}

	public static ArrayList<String> buildWeek() {
		ArrayList<String> dates = new ArrayList<String>();
		for (int i = 0; i < 20; i++) {
			dates.add(buildWeekString(i));
		}
		return dates;
	}

	private static String buildWeekString(int weekDiff) {
		cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"), Locale.GERMANY);
		cal.add(Calendar.WEEK_OF_YEAR, weekDiff);

		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());

		String tag = "" + cal.get(Calendar.DAY_OF_MONTH);
		String monat = "" + (cal.get(Calendar.MONTH) + 1);

		String montag = ((tag.length() < 2) ? "0" + tag : tag) + "."
				+ ((monat.length() < 2) ? "0" + monat : monat) + "."
				+ cal.get(Calendar.YEAR);

		montag = sdf.format(cal.getTime());

		cal.add(Calendar.DAY_OF_MONTH, 6);
		String sonntag = sdf.format(cal.getTime());

		return montag + " - " + sonntag;
	}

	public static String getDayOfTheWeek(int i) {
		String setTag = "";
		cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		switch (i) {
		case 1:
			setTag = "Montag";
			break;
		case 2:
			setTag = "Montag";
			break;
		case 3:
			setTag = "Dienstag";
			break;
		case 4:
			setTag = "Mittwoch";
			break;
		case 5:
			setTag = "Donnerstag";
			break;
		case 6:
			setTag = "Freitag";
			break;
		case 7:
			setTag = "Samstag";
			break;
		}
		return setTag;
	}

	public static String getDayOfTheWeek() {
		String setTag = "";
		cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		switch (cal.get(Calendar.DAY_OF_WEEK)) {
		case 1:
			setTag = "Montag";
			break;
		case 2:
			setTag = "Montag";
			break;
		case 3:
			setTag = "Dienstag";
			break;
		case 4:
			setTag = "Mittwoch";
			break;
		case 5:
			setTag = "Donnerstag";
			break;
		case 6:
			setTag = "Freitag";
			break;
		case 7:
			setTag = "Samstag";
			break;
		}
		return setTag;
	}

	public static String getKW() {

		return "" + cal.get(Calendar.WEEK_OF_YEAR);
	}

	public static String getDate() {
		cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));

		return sdf.format(cal.getTime());
	}
}
