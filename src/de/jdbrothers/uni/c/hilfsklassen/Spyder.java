package de.jdbrothers.uni.c.hilfsklassen;

import java.io.IOException;
import java.net.ConnectException;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.app.Activity;
import android.widget.Toast;

public class Spyder extends Activity {
	private static String useragent = "Mozilla";

	public Map<String, String> getSession(String loginurl, String[] condata,
			String[] conkeys, Method method) throws IOException {
		Connection con = Jsoup.connect(loginurl);

		T.s("Spyder", "Baue Verbindung zu" + loginurl + " auf.");
		T.s("Spyder", "Datenl�nge: " + condata.length);
		T.s("Spyder", "Schl�ssell�nge: " + conkeys.length);

		for (int i = 0; i < conkeys.length - 1; i++) {
			con.data(conkeys[i], condata[i]);
			T.s("Spyder", "setze con.data() - "+conkeys[i] + " : " + condata[i]);
		}

		// David --> dynamisch aus db auslesen
//		SQLiteDatabase db = new DBManager(datasave.TempContext)
//				.getReadableDatabase();
//		Cursor cursor = db.rawQuery("Select " + DBDaten.T_Daten_S_TimeOut
//				+ " from " + DBDaten.T_Daten_Name, null);
//		cursor.moveToFirst();

		con.timeout(
				 datasave.timeout
//				cursor.getInt(0)
				); 
		con.userAgent(useragent);
		con.method(method);
		Response r = con.execute();

//		cursor.close();
//		db.close();
		// <-- Dynamisch auslesen ende

		if (r.cookies() == null) {
			// Verbindungsfehler
			throw new ConnectException("can't get cookies");
		} else {
			return r.cookies();
		}
	}

	public Document getDoc(String loginurl, String dataurl, String[] conkeys,
			String[] condata, int timeout, Method method,
			String errorExpression, String errorResult) {
		Map<String, String> cookies = null;
		

		try {

			if (cookies == null) {
				cookies = getSession(loginurl, condata, conkeys, method);
				datasave.cookie = cookies;
				datasave.cookieerror(false);
			} // else{}; auch wenn der cookie angelegt wird sollte doch gleich
				// die richtige seite mit geholt werden
			Document document = Jsoup.connect(dataurl)
					.cookie("PHPSESSID", cookies.get("PHPSESSID")).get();

			// Internetverbindung �berpr�fen
			if (document == null)
				throw new ConnectException("can't get html");

			// Check Document (Session abgelaufen)
			if (document.select(errorExpression).contains(errorResult)) {
				cookies = getSession(loginurl, condata, conkeys, method);
				datasave.cookie = cookies;
				datasave.cookieerror(false);

				document = Jsoup.connect(dataurl)
						.cookie("PHPSESSID", cookies.get("PHPSESSID")).get();

				// Check Document (PW ung�ltig)
				if (document.select(errorExpression).contains(errorResult)) {
					throw new Exception("wrong user or password");
				}
			}

			// Resultat zur�ckgeben
			return document;

		} catch (Exception e) { // DAS MUSS NAT�RLICH AUSGEGEBEN WERDEN
			e.printStackTrace();
			Toast.makeText(datasave.TempContext, "Fehler im Spyder !",
					Toast.LENGTH_SHORT);
			return null;
		}

	}

	public static boolean post(String URL, String[] keys, String[] data) {
		boolean returnvalue = false;
		try {
			Connection con = Jsoup.connect(URL);
			for (int i = 0; i < keys.length - 1; i++) {
				con.data(keys[i], data[i]);
			}

			con.timeout(datasave.timeout); // 5000
			con.userAgent(useragent);
			con.method(Method.POST);
			con.followRedirects(true);
			con.execute();

			returnvalue = true;
		} catch (Exception e) {
			returnvalue = false;
			e.printStackTrace();
		}

		return returnvalue;
	}
}
