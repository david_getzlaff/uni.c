package de.jdbrothers.uni.c.hilfsklassen;

import java.util.Map;

import android.app.Activity;
import android.content.Context;
import de.jdbrothers.uni.c.db.DBAPI;

public class datasave extends Activity {

	// Tempor�re Daten - David
	public static String Temp_Nr = "";
	public static String Temp_PW = "";
	public static int Temp_TimeOut = 5000;
	public static int Temp_TimeOut_Position = 5;

	public static String Nummer_Alt;

	public static Context TempContext = null;

	// Alex Daten
//	public static String loginname = "";
//	public static String loginpassword = "";
	public static String conkeys[] = { "matrikelnr", "passwort", "submit" };
	public static String condata[] = { Temp_PW, Temp_Nr, "LOGIN" };
	public static int timeout;
	public static String selectetuniversity;
	public static int test = 0;
	public static Map<String, String> cookie = null;
	public static boolean cookieerror = false;
	public static boolean rememberdata = true;

	public static int uniselectionid = 0;
	public static int timeoutselectionid = 0;

	public static String mensaselectedtitle;
	public static String mensaselecteddescription;
	public static String mensaselectedinfo;

	public static String contactselectedtelefon;
	public static String contactselectedemail;
	public static String contactselectedaddress;
	public static String contactselectedname;

	public static String mensaname;
	public static String mensaday;

	public static void refresh(Context c) {
		// Daten aus DB Laden
		if (DBAPI.getLoginData(c)[0].equals("000")
				&& DBAPI.getLoginData(c)[1].equals("")) {
			// MatrNr && PW nicht in DB
			condata[0] = Temp_Nr;
			condata[1] = Temp_PW;
			timeout = Temp_TimeOut;
		} else {
			// MatrNr && PW in DB
			condata[0] = DBAPI.getLoginData(c)[0];
			condata[1] = DBAPI.getLoginData(c)[1];
			timeout = Integer.parseInt(DBAPI.getLoginData(c)[2]);
		}
	}

	public static void cookieerror(boolean value) {
		cookieerror = value;
	}

	public static void cleanPhoneNumber() {
		// implementieren!!!!
	}

	public static String[] getCondata(Context c) {
		String[] tmp = DBAPI.getLoginData(c);
		condata[0] = tmp[0];
		condata[1] = tmp[1];
		condata[2] = "LOGIN";
		timeout = Integer.parseInt(tmp[2]);
		return condata;
	}
	public static String[] getConkeys() {
		return conkeys;
	}

}
