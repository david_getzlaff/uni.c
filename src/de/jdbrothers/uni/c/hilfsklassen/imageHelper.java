package de.jdbrothers.uni.c.hilfsklassen;

import java.io.BufferedInputStream;
import java.net.URL;

import org.apache.http.util.ByteArrayBuffer;

public class imageHelper {

	public static ByteArrayBuffer saveImage(URL url) {

		ByteArrayBuffer bufferArrayStream = new ByteArrayBuffer(512);
		try {
			BufferedInputStream bufferInputStream = new BufferedInputStream(url
					.openConnection().getInputStream(), 512);
			int integer = 0;
			while ((integer = bufferInputStream.read()) != -1) {
				bufferArrayStream.append((byte) integer);
			}

		} catch (Exception e) {
			T.e("imageHelper", e);
		}
		return bufferArrayStream;
	}
}
