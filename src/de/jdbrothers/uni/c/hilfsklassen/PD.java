package de.jdbrothers.uni.c.hilfsklassen;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import de.jdbrothers.uni.c.ContactActivity;
import de.jdbrothers.uni.c.MensaActivity;
import de.jdbrothers.uni.c.NewsActivity;
import de.jdbrothers.uni.c.NotenActivity;
import de.jdbrothers.uni.c.PlanActivity;
import de.jdbrothers.uni.c.feedbackActivity;
import de.jdbrothers.uni.c.db.DBAPI;

public class PD extends Thread implements Runnable {

	private static ProgressDialog myPD;
	private static Context c;
	private static String message;
	private static String name;

	public static void runIT(Activity a, String s) {
		c = a.getApplicationContext();
		message = s;
		name = a.getTitle().toString();
		myPD = ProgressDialog.show(a, "Bitte warten", message, false, true);
		new PD().start();
	}

	@Override
	public void run() {
		Looper.prepare();

		if(name.equals(newMenu.feedback)){
			
			Connection con =Jsoup.connect(feedbackActivity.URL);
				
			for (int i = 0; i < feedbackActivity.keys.length; i++) {
				con.data(feedbackActivity.keys[i], feedbackActivity.data[i]);
				System.out.println(feedbackActivity.keys[i]+" : "+feedbackActivity.data[i]);
			}
			
			con.method(Method.POST);
			try{				
				Document doc = con.execute().parse();
				System.out.println(doc.text());
				
				if(doc.hasText()){
					System.out.println(doc.text());
				}
			}catch(Exception e){
				System.out.println("FEHLER BEIM POSTEN");
			}
			
			myHandler.sendEmptyMessage(0);
		}
		
		if (name.equals(newMenu.notenA)) {
			NotenActivity.NotenHerunterladenUndSpeichern(c);
			myHandler.sendEmptyMessage(1);
		}

		if (name.equals(newMenu.newsA)) {
			NewsActivity.NewsHerunterladenUndSpeichern(c);
			myHandler.sendEmptyMessage(2);
		}

		if (name.equals(newMenu.contactA)) {
			ContactActivity.KontakteHerunterladenUndSpeichern(c);
			myHandler.sendEmptyMessage(3);
		}

		if (name.equals(newMenu.mensaA)) {
			MensaActivity.getFood(c);
			myHandler.sendEmptyMessage(4);
		}

		if (name.equals(newMenu.planA)) {
			PlanActivity.ladeDaten(c, PlanActivity.sp.getSelectedItem().toString());
			myHandler.sendEmptyMessage(5);
		}
		
		Looper.loop();
	}

	private Handler myHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			switch (Integer.parseInt(msg.toString().substring(7, 8))) {
			case 0:
				//Feedback
			T.t(c, "Vielen Dank f�r Ihr Feedback");
				break;
				
			case 1:
				// Noten
				NotenActivity.filter(c, "" + DBAPI.getFilterpos(c));
				T.t(c, "Noten aktualisiert");
				break;

			case 2:
				// News
				NewsActivity.filter(c, "" + DBAPI.getFilterpos(c));
				T.t(c, "News aktualisiert");
				break;

			case 3:
				// Kontaktdaten
				ContactActivity.listContacts(c);
				T.t(c, "Kontakte aktualisiert");
				break;

			case 4:
				// Mampfen !
				MensaActivity.handleIT(c);
				break;
			
			case 5:
				//Stundenplan
				 PlanActivity.showPlan(dateWorker.getDayOfTheWeek());
				break;
			}
			myPD.dismiss();
			super.handleMessage(msg);
		}
	};
}
