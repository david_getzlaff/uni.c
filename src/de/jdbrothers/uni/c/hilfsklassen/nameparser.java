package de.jdbrothers.uni.c.hilfsklassen;

public class nameparser {
private String anrede="";
private String titel="";
private String vorname="";
private String nachname="";
int point=0;
	
	public nameparser(String name){
		String tmpstring="";
		
		
		for(int i=0;i<name.length();i++){
			if(point<=i){	
				
			tmpstring=name.substring(point,i);
			
			
			if(tmpstring.equalsIgnoreCase("Herr")){
				anrede="Herr";
				point=Integer.parseInt(""+i);				
			}
			
			if(tmpstring.equalsIgnoreCase("Frau")){
				anrede="Frau";
				point=i;
			}
			
			if(tmpstring.contains(".")){
				titel=titel+tmpstring;
				point=i;
			}
			
		}}
		
		nachname=name.substring(name.lastIndexOf(" "),name.length());
		name=name.substring(0,name.lastIndexOf(" "));
		vorname=name.substring(point,name.length());		
		
	}
	
	public String getTitle(){
		return titel;
	}
	public String getAnrede(){
		return anrede;
	}
	public String getVorname(){
		return vorname;
	}
	public String getNachname(){
		return nachname;
	}
	
	public String getMailHead(){
		String mailstring="Sehr geehrte";		
		if(anrede.equalsIgnoreCase("herr")){
			mailstring=mailstring+"r Herr ";
		}else{
			mailstring=mailstring+" Frau ";
		}
		mailstring=mailstring+titel;
		mailstring=mailstring+nachname;
		mailstring=mailstring+",\n";
		
		return mailstring;		
		
	}
	
}

