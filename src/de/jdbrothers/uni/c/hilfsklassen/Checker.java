package de.jdbrothers.uni.c.hilfsklassen;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;

import android.content.Context;

public class Checker {

	public static boolean Check(String[] checkstring) {
		boolean returnvalue = false;

		try {
			for (int i = 0; i < checkstring.length; i++) {
				if (checkstring[i].length() >= 1) {
					returnvalue = true;
					break;
				}
			}
		} catch (Exception e) {

		}
		return returnvalue;
	}

	public static boolean Check(String[][] checkstring) {
		boolean returnvalue = false;

		try {
			for (int i = 0; i < checkstring.length; i++) {
				for (int j = 0; j < checkstring[i].length; j++)
					if (checkstring[i][j].length() >= 1) {
						returnvalue = true;
						break;
					}
			}
		} catch (Exception e) {

		}
		return returnvalue;
	}

	public static boolean Check(String checkstring) {
		boolean returnvalue = false;

		try {
			if (checkstring.length() >= 1) {
				returnvalue = true;
			}
		} catch (Exception e) {

		}
		return returnvalue;
	}

	public static boolean checkBetaDate(Context c) {
		boolean tmp = false;
		String checkstring = "LastChecked";

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeZone(TimeZone.getTimeZone("GMT"));
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String Datum_System = sdf.format(cal.getTime());// Systemdatum
		// if(DB_API.getData(this,
		// DatenbankDaten.T_Settings_Name,DatenbankDaten.T_Settings_S_Wert,DatenbankDaten.T_Settings_S_Attribut+" like '%"+checkstring+"%'")[0][0]!=Datum_System)

		String dbdatum = DBAPI.getData(datasave.TempContext,
				DBDaten.T_Settings_Name,
				DBDaten.T_Settings_S_Wert,
				DBDaten.T_Settings_S_Attribut + " like '%" + checkstring
						+ "%'")[0][0];

		if (dbdatum.equalsIgnoreCase(Datum_System)) {
			tmp = true;
		} else {
			try {
				DBAPI.delete(c, DBDaten.T_Settings_Name,
						DBDaten.T_Settings_S_Attribut + " like '%"
								+ checkstring + "%'");  // Lastchecked l�schen
				
				
				System.out.println("Pr�fe auf Beta-end-Datum");
				String url = "https://dl.dropbox.com/u/56976000/date.txt";

				String readline = "";
				BufferedReader buff = new BufferedReader(new InputStreamReader(
						new URL(url).openStream(), "iso-8859-1"));

				if ((readline = buff.readLine()) != null) {
					System.out.println("heute: " + Datum_System
							+ ", Ablaufdatum: " + readline);
					long loadedTime = sdf.parse(readline).getTime();

					// Differenz
					long diff = loadedTime - sdf.parse(Datum_System).getTime();
					if (diff < 0) {
						tmp = false;
					} else{
						tmp = true;

					
					String[][] data = {
							{ DBDaten.T_Settings_S_Attribut,
									DBDaten.T_Settings_S_Wert },
							{ checkstring, Datum_System } };
					DBAPI.insertInto(c, DBDaten.T_Settings_Name,
							data);
				}}

			} catch (Exception e) {
				tmp=false;
			}
		}
		return tmp;
	}	
	
	
	

	
}
