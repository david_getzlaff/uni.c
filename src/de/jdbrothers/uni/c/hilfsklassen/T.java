package de.jdbrothers.uni.c.hilfsklassen;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class T extends Activity {
	
	//Toasts=========================================================================
	public static void t(Context c, String toast) {
		Toast.makeText(c, toast, Toast.LENGTH_SHORT).show();
	}
	//SYSOs=========================================================================
	public static void s(String activityName, String meldung) {
		System.out.println("_" + activityName + "_ >>>> " + meldung);
	}

	public static void s(String activityName, CharSequence meldung) {
		s(activityName, meldung.toString());
	}

	public static void s(Activity activityName, String title) {
		s(activityName.getTitle().toString(), title);
	}
	
	public static void s(Activity activityName, CharSequence title) {
		s(activityName.getTitle().toString(), (String) title);
	}

	//Exceptions=========================================================================
	public static void e(String activityName, Exception e) {
		System.out.println("------<" + activityName + ">------");
		e.printStackTrace();
		System.out.println("------<" + activityName + "/>-----");
	}
	public static void e(Activity activityName, Exception e){
		e(activityName.getTitle().toString(), e);
	}

	//LOGs=========================================================================
	public static void l(String tag, String msg) {
		Log.d(tag, msg);
	}


}
