package de.jdbrothers.uni.c.hilfsklassen;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import android.content.Context;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.dateWorker;

public class planImporter {
	static Document document;
	static String[][] dates;
	static ArrayList<String> set_dat = dateWorker.buildWeek();

	private static String dataurl = "http://www.ba-dresden.de/index2.php?menu=5&item=22";

	static String parseTime = "html body div#world div#Page2 "
			+ "table tbody tr td div#PageContent2 div#PageSubContent2a form "
			+ "table tbody tr td";

	static String parsePlan = "html body div#world div#Page2 "
			+ "table tbody tr td div#PageContent2 div#PageSubContent2a form "
			+ "table tbody tr td table tbody tr";

	static Map<String, String> co;

	static Context c;

	public static boolean importIT(Context _c, String woche) {
		boolean ret = false;
		long t1 = System.currentTimeMillis();
		c = _c;

		if (login() == true) {
			go2Week(woche);
			ret = true;
		}

		System.out.println("-----\nDone - ben�tigte Zeit: "
				+ (System.currentTimeMillis() - t1));
		return ret;
	}

	public static boolean login() {
		try {
			org.jsoup.Connection.Response response = Jsoup.connect(dataurl)
					.data("matrikelnr", datasave.condata[0])
					.data("passwort", datasave.condata[1])
					.data("submit", "LOGIN").userAgent("Mozilla")
					.timeout(datasave.timeout).method(Method.POST).execute();

			co = response.cookies();

			document = Jsoup.connect(dataurl)
					.cookie("PHPSESSID", co.get("PHPSESSID")).get();

			// System.out.println("Einloggen erfolgreich als: "
			// + datasave.condata[0]);
			return true;
		} catch (Exception e) {
			System.out.println("Einloggen fehlgeschlagen");
			T.t(c, "Einloggen fehlgeschlagen !");
			e.printStackTrace();
			return false;
		}
	}

	private static void go2Week(String woche) {
		try {
			// Change week
			org.jsoup.Connection.Response response;
			response = Jsoup.connect(dataurl).data("sel_dat", woche)
					.userAgent("Mozilla").timeout(datasave.timeout)
					.cookie("PHPSESSID", co.get("PHPSESSID"))
					.method(Method.POST).execute();
			document = Jsoup.parse(response.body());

		} catch (Exception e) {
			T.s("planImporter", "Fehler beim Wechseln der Woche");
			T.e("planImporter", e);
		}

		getTime();
		getplan();
		DBAPI.insertInto(c, DBDaten.T_Plan_Name, new String[] { "KW", "Datum",
				"Von", "Bis", "Montag", "Dienstag", "Mittwoch", "Donnerstag",
				"Freitag", "Samstag" }, dates);
	}

	private static void getTime() {
		// Zeiten auslesen und in String[][] packen
		Iterator<org.jsoup.nodes.Element> ite = ((org.jsoup.nodes.Element) document)
				.select(parseTime).iterator();

		ite.next(); // kopfzeile �berspringen
		String[] t = (ite.next().text()).split(" ");

		String kw = t[0].replace(".", "");

		String date = t[3] + " - " + t[5];
		ite.next();
		ite.next();
		ite.next();
		ite.next();
		ite.next();
		ite.next();
		ite.next();
		ite.next();
		// Header auf BA-Seite �bersprungen
		int zeilen = 0;
		ArrayList<String> tmp = new ArrayList<String>();

		while (ite.hasNext()) { // Uhrzeiten
			Element e = ite.next();
			String zeiten = e.ownText();
			if (zeiten.contains(":") && zeiten.contains(" - ")) {
				tmp.add(zeiten);
				zeilen++;
			}
		}// While
		dates = new String[zeilen][10];
		for (int i = 0; i < tmp.size(); i++) {
			String[] s = tmp.get(i).replace(" ", "").split("-");
			dates[i][0] = kw;
			dates[i][1] = date;
			dates[i][2] = s[0];
			dates[i][3] = s[1];
		}
	}

	public static void getplan() {
		Iterator<org.jsoup.nodes.Element> ite = ((org.jsoup.nodes.Element) document)
				.select(parsePlan).iterator();
		int spalte = 0;
		int zeile = 0;
		String Module = "";
		while (ite.hasNext()) { // Module
			Module = ite.next().text();
			if (!Module.contains("Anzuzeigende Woche w�hlen")) {
				if (Module.length() == 0)
					Module = "-";
				dates[zeile][spalte + 4] = Module;
			}
			if (spalte < 5)
				spalte++;
			else {
				spalte = 0;
				zeile++;
			}
		}// While
	}

	public static ArrayList<String> getSet_dat() {
		return set_dat;
	}
}
