package de.jdbrothers.uni.c.hilfsklassen;

import java.util.ArrayList;
import java.util.HashMap;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ListFiller extends Activity {

	public static SimpleAdapter sa;
	private static ArrayList<HashMap<String, String>> arrayList;
	private static String[] myItems;

	// Erstellt dynamsch die Items====================================
	private static void setitems(int[] objects) {
		myItems = new String[objects.length];
		for (int i = 0; i < objects.length; i++) {
			myItems[i] = "item_" + i;
		}
	}

	// Erstellt Liste=================================================
	public static SimpleAdapter createAdapter(Context c, int layout,
			int[] objects) {
//		T.s("ListFiller", "create Adapter");
		arrayList = new ArrayList<HashMap<String, String>>();
		setitems(objects);
		sa = new SimpleAdapter(c, arrayList, layout, myItems, objects);
		return sa;
	}

	// Befuellt Liste=================================================

	public static void fill(Context c, String[][] daten) {
//		T.s("ListFiller", "fill List");
		arrayList.clear();
		HashMap<String, String> item = new HashMap<String, String>();

		for (int zeile = 0; zeile < daten.length; zeile++) {
			for (int spalte = 0; spalte < daten[0].length; spalte++) {
				item.put(myItems[spalte], daten[zeile][spalte]);
			}
			arrayList.add(item);
			item = new HashMap<String, String>();
		}
		sa.notifyDataSetChanged();
		
	}
}
