package de.jdbrothers.uni.c.hilfsklassen;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import de.jdbrothers.uni.c.NewsActivity;
import de.jdbrothers.uni.c.NotenActivity;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;

public class AD extends Activity {

	public static String deleteTable = "deleteTable";
	public static String updateTable = "updateTable";
	public static String userHasChanged = "userHasChanged";

	public static void ring(final Context c, final String Befehl,
			final String Achtung, final String Tabelle, final String[] newData) {
		AlertDialog.Builder alert = new AlertDialog.Builder(c);
		alert.setTitle(Achtung).setPositiveButton("Ja", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				try {
					if (Befehl.equals(deleteTable)) {
						DBAPI.delete(c, Tabelle);
						T.t(c, "Daten gel�scht.");
					}

					if (Befehl.equals(updateTable)) {
						// DBAPI.DBUpdate(c, DBDaten.T_Daten_Name, UpdateWerte);
						DBAPI.updateLoginData(c, newData);
						((Activity) c).finish();
					}

					if (Befehl.equals(userHasChanged)) {
						// User �ndern
						DBAPI.updateLoginData(c, newData);
						// DBAPI.DBUpdate(c, DBDaten.T_Daten_Name, UpdateWerte);
						// L�sche folgende Tabellen:
						// News
						// Noten
						// Kontakte
						//Stundenplan
						DBAPI.delete(c, DBDaten.T_Noten_Name);
						DBAPI.delete(c, DBDaten.T_News_Name);
						DBAPI.delete(c, DBDaten.T_Kontakt_Name);
						DBAPI.delete(c, DBDaten.T_Plan_Name);
						((Activity) c).finish();
						T.t(c, "Daten ge�ndert.");
					}
				} catch (Exception e) {
					T.e("AD", e);
				}
			}
		}).setNegativeButton("Nein", new OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				T.t(c, "Daten nicht ver�ndert.");
			}
		}).show();
	}

	public static void showAlert(Context c, String s) {
		AlertDialog.Builder alert = new AlertDialog.Builder(c);
		alert.setTitle(s);
		alert.setPositiveButton("OK", null).show();
	}

	public static void filter(final Context c, final String titel,
			String table, String toGroup) {

		String[][] tmp = DBAPI.getData(c, table, toGroup, " where " + toGroup
				+ " not null Group By " + toGroup);

		// Pr�fung auf Leerstellen
		List<String> stringList = new ArrayList<String>();

		for (int i = 0; i < tmp.length; i++) {
			if (tmp[i][0].length() > 0) {
				stringList.add(tmp[i][0]);
			}
		}
		// Pr�fung ende

		final String[] items = stringList
				.toArray(new String[stringList.size() + 1]);

		items[0] = "kein Filter";
		for (int i = 1; i < items.length; i++)
			items[i] = stringList.get(i - 1);

		System.out.println("Items.length: " + items.length);

		AlertDialog.Builder builder = new AlertDialog.Builder(c);
		builder.setTitle(titel);

		System.out.println("Baue AD");

		builder.setSingleChoiceItems(items, DBAPI.getFilterpos(c),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {

						DBAPI.setFilterpos(c, item);

						if (titel.equals(newMenu.filterNachSemester)) {
							NotenActivity.filter(c, "" + item);
						}
						if (titel.equals(newMenu.filterNachAbsender)) {
							System.out
									.println("Absender gew�hlt - items[item]: "
											+ items[item]);
							NewsActivity.filter(c, items[item]);

						}
						dialog.cancel();
					}
				}).show();
	}
}
