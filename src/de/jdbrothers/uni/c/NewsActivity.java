package de.jdbrothers.uni.c;

import java.util.ArrayList;
import java.util.Iterator;

import org.jsoup.Connection.Method;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.db.DBManager;
import de.jdbrothers.uni.c.hilfsklassen.AD;
import de.jdbrothers.uni.c.hilfsklassen.Checker;
import de.jdbrothers.uni.c.hilfsklassen.ListFiller;
import de.jdbrothers.uni.c.hilfsklassen.PD;
import de.jdbrothers.uni.c.hilfsklassen.Spyder;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.newMenu;

public class NewsActivity extends Activity implements Runnable {

	public static String loginurl = "http://www.ba-dresden.de/index2.php?menu=5";
	public static String dataurl = "http://www.ba-dresden.de/index2.php?menu=5&item=20";
	public static String errorExpression = "html body div#world div#Page2 table tbody tr td div#PageContent2 div#PageSubContent2 h6";
	public static String errorResult = "Sie m�ssen sich erst anmelden!";

	public static ProgressDialog pd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news);

		setTitle(newMenu.newsA);

		ListView liste = (ListView) findViewById(R.id.listView1);
		liste.setAdapter(ListFiller.createAdapter(this, R.layout.news_item,
				new int[] { R.id.text1, R.id.text2, R.id.text3 }));

		int AlteDatens�tze = DBAPI.countRows(this, DBDaten.T_News_Name);

		if (AlteDatens�tze == 0) {
			PD.runIT(this, "Lade News");

		} else

			filter(this, "" + DBAPI.getFilterpos(this));

		pr�feAufNeueNews(AlteDatens�tze);

	}

	public static void filter(Context c, String zahl) {
		if (zahl.equals("0") || zahl.equals("kein Filter")) {
			zahl = "%";
		}

		ListFiller.fill(c, getTheseData(c, zahl));

	}

	private static String[][] getTheseData(Context c, String operator) {

		// datum, absender, message
		return DBAPI.getData(c, DBDaten.T_News_Name, new String[] {
				DBDaten.T_News_S_Datum, DBDaten.T_News_S_Absender,
				DBDaten.T_News_S_Nachricht }, " where "
				+ DBDaten.T_News_S_Absender + " like '%" + operator + "%'");
	}

	public static void NewsHerunterladenUndSpeichern(Context c) {

		Iterator<Element> ite = null;
		if (Checker.checkBetaDate(c)) {
			try {
				Document document = new Spyder().getDoc(loginurl, dataurl,
						datasave.conkeys, datasave.getCondata(c),
						datasave.timeout, Method.POST, errorExpression,
						errorResult);

				document.select("html body div#world div#Page2 table tbody tr td div#PageContent2 div#PageSubContent2 form table.content_l tbody tr td table tbody");

				ite = document
						.select("html body div#world div#Page2 table tbody tr td div#PageContent2 div#PageSubContent2 form table.content_l tbody tr td table tbody tr td")
						.iterator();
				ite.next();
				ite.next();
				ite.next();
				ite.next();
				ite.next(); // erste Zeile �berspringen

			} catch (Exception e) {
				System.out.println("Fehler bei Document oder Iterator");
			}

			if (ite != null) {

				Integer n = 0;
				ContentValues News = new ContentValues();

				ArrayList<ContentValues> bob = new ArrayList<ContentValues>();

				try {
					while (ite.hasNext()) {
						if (n == 0 || n == 4) { // 0 = gelesen; 4 = Checkbox
							ite.next();
						} else {
							if (n == 1) {
								String Zeile = ite.next().text();
								News.put(DBDaten.T_News_S_Datum, Zeile);
							}
							if (n == 2) {
								String Zeile = ite.next().text();
								News.put(DBDaten.T_News_S_Absender, Zeile);
							}
							if (n == 3) {
								String Zeile = ite.next().text();
								News.put(DBDaten.T_News_S_Nachricht, Zeile);
							}
						}
						if (n == 4) {
							bob.add(News);
							News = new ContentValues();
							n = 0;
						} else {
							n++;
						}
					}
					if (bob.size() > 1) {

						DBAPI.delete(c, DBDaten.T_News_Name);

						SQLiteDatabase db = new DBManager(c)
								.getReadableDatabase();

						for (int i = 0; i < bob.size(); i++) {
							db.insert(DBDaten.T_News_Name, null, bob.get(i));
						}
						db.close();
					}

					else {
						T.t(c, "Nicht �h��hh���");
					}

				} catch (Exception e) {
					System.out.println("Herunterladen-Catch");
					datasave.cookieerror(true);
				}
			}
		} else {
			AD.showAlert(c, "Beta Abgelaufen");

		}
	}

	// public static void NewsLadenUndDarstellen(Context c) {
	// myListing.clear();
	// SQLiteDatabase db = new DBManager(c).getReadableDatabase();
	//
	// Cursor getNews = db.rawQuery(DBDaten.getNews_RAW, null);
	//
	// String[] News = { "Datum", "Absender", "Nachricht" };
	//
	// if (getNews.getCount() != 0) {
	//
	// while (getNews.moveToNext()) {
	// for (int s = 0; s < getNews.getColumnCount(); s++)
	// try {
	// News[s] = getNews.getString(s);
	// } catch (Exception e) {
	// T.toast(c, "Fehler beim Konvertieren");
	// }
	// item.put("Datum", News[0]);
	// item.put("Absender", News[1]);
	// item.put("Nachricht", News[2]);
	// myListing.add(item);
	// item = new HashMap<String, String>();
	// }
	//
	// } else {
	// T.toast(c, "Keine Noten in der Datenbank gefunden");
	// }
	// saList.notifyDataSetChanged();
	// getNews.close();
	// db.close();
	//
	// }

	public void pr�feAufNeueNews(int alteDatens�tze) {
		if (DBAPI.countRows(this, DBDaten.T_News_Name) > alteDatens�tze) {
			T.t(this, "Yo, neue News :D");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(newMenu.createMenu(menu, new String[] {
				newMenu.neuLaden, newMenu.filter, newMenu.hauptmen� }));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(newMenu.getAction(item, this));
	}

	@Override
	public void run() {

		Looper.prepare();

		NewsHerunterladenUndSpeichern(NewsActivity.this);
		System.out.println("Daten heruntergeladen ^^");
		myHandler.sendEmptyMessage(0);

		Looper.loop();

	}

	private Handler myHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			pd.dismiss();

			filter(NewsActivity.this,
					"" + DBAPI.getFilterpos(NewsActivity.this));
			T.t(NewsActivity.this, "News aktualisiert");

			super.handleMessage(msg);
		}

	};

	public void onClick(View v) {
		String shareText = "Ich habe im ";
		String s = "";
		LinearLayout main = (LinearLayout) v;
		String datum = ((TextView) main.getChildAt(0)).getText().toString();
		String absender = ((TextView) main.getChildAt(1)).getText().toString();
		String nachricht = ((TextView) main.getChildAt(2)).getText().toString();

		shareText = "Am " + datum + " schrieb " + absender
				+ " folgende Nachricht:\n\"" + nachricht + "\"";

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_TEXT, shareText);
		startActivity(Intent.createChooser(i, "Share"));
	}

}