package de.jdbrothers.uni.c;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.AD;
import de.jdbrothers.uni.c.hilfsklassen.newMenu;

public class ResetMenueActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resetmenue);
		
		setTitle(newMenu.resetA);
	}

	public void onDeleteLoginDatenClick(View v) {
		datasave.cookie=null;
		AD.ring(this, AD.deleteTable, getResources().getString(R.string.String_LoginDaten)+" wirklich l�schen?", "", null);
	}

	public void onDeleteCacheClick(View v) {
		datasave.cookie=null;
		T.t(this, "Cookies gel�scht.");
	}

	public void onDeleteNewsClick(View v) {
		AD.ring(this, AD.deleteTable, getResources().getString(R.string.str_news)+" wirklich l�schen?", DBDaten.T_News_Name, null);
	}

	public void onDeleteContactClick(View v) {
		AD.ring(this, AD.deleteTable, getResources().getString(R.string.str_ansprechpartner)+" wirklich l�schen?", DBDaten.T_Kontakt_Name, null);
	}

	public void onDeleteErgebnisseClick(View v) {
		AD.ring(this, AD.deleteTable, getResources().getString(R.string.str_note)+" wirklich l�schen?", DBDaten.T_Noten_Name, null);
	}
	public void onDeleteEssenClick(View v) {
		AD.ring(this, AD.deleteTable, getResources().getString(R.string.str_speiseplan)+" wirklich l�schen?", DBDaten.T_Angebot_Name, null);
	}
	public void onDeleteStundenplanClick(View v) {
		AD.ring(this, AD.deleteTable, getResources().getString(R.string.str_stundenplan)+" wirklich l�schen?", DBDaten.T_Plan_Name, null);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(newMenu.createMenu(menu, new String[]{newMenu.hauptmen�}));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(newMenu.getAction(item, this));
	}
	
	
}
