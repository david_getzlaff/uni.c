package de.jdbrothers.uni.c;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.db.s;
import de.jdbrothers.uni.c.hilfsklassen.ListFiller;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.log;

public class NotenschluesselActivity extends Activity {

	static ListView view;
	int AlteDatensätze;

//	ListAdapter sa;

	static HashMap<String, String> item;
	static ArrayList<HashMap<String, String>> myListing;

	static String[] modul;
	static String[] verteilung;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notenschluessel);

		this.setTitle("Notenschlüssel");
		
		view = (ListView) findViewById(R.id.listViewNotenschluessel);
		view.setFastScrollEnabled(true);
		view.setAdapter(ListFiller.createAdapter(this,
				R.layout.notenschluessel_item, new int[] { R.id.tv_semester,
						R.id.tv_modul, R.id.et_schluessel }));

		VerteilungLadenUndDarstellen(this);

		view.getFirstVisiblePosition();

		// ((EditText) view.getAdapter().getView(0, view.getChildAt(0),
		// view).findViewById(R.id.et_schluessel)).requestFocus();

		// if (view.getCount() > 1) {
		// view.setSelection(0);
		// ((TextView) view.getAdapter().getView(// Liste
		// 2,
		// // element in der liste
		// view.getChildAt(0),
		// // Parent
		// view).findViewById(R.id.et_schluessel))
		// .setSelectAllOnFocus(true);
		// }

//		sa = (ListAdapter) view.getAdapter();
//		AlteDatensätze = view.getCount();

//		for (int i = 0; i < AlteDatensätze; i++) {
//
//			View v = view.getAdapter().getView(i, view.getChildAt(0), view);
//
//			modul = verteilung = new String[AlteDatensätze];
//			modul[i] = ((EditText) v.findViewById(R.id.et_schluessel))
//					.getText().toString();
//			verteilung[i] = ((TextView) v.findViewById(R.id.tv_modul))
//					.getText().toString() + "%'";
//			// new s("i: "
//			// + i
//			// + "  "
//			// + ((TextView) v.findViewById(R.id.tv_semester)).getText()
//			// .toString());
//
//			// new s("i: "
//			// + i
//			// + " --> "
//			// + ((TextView) v.findViewById(R.id.tv_modul)).getText()
//			// .toString());
//
//			// new s("i: "
//			// + i
//			// + "  "
//			// + ((EditText) v.findViewById(R.id.et_schluessel)).getText()
//			// .toString() + "%");
//		}

	}

	public static void VerteilungLadenUndDarstellen(Context c) {

		ListFiller.fill(c, DBAPI.getData(c, DBDaten.T_Noten_Name, new String[] {
				"Semester", "Modul", "Gewichtung" },
				"where note not like 'null' Order by semester"));
		
		for (int i = 0; i < view.getCount(); i++) {

			View v = view.getAdapter().getView(i, view.getChildAt(0), view);

			modul = verteilung = new String[view.getCount()];
			modul[i] = ((EditText) v.findViewById(R.id.et_schluessel))
					.getText().toString();
			verteilung[i] = ((TextView) v.findViewById(R.id.tv_modul))
					.getText().toString() + "%'";
			// new s("i: "
			// + i
			// + "  "
			// + ((TextView) v.findViewById(R.id.tv_semester)).getText()
			// .toString());

			// new s("i: "
			// + i
			// + " --> "
			// + ((TextView) v.findViewById(R.id.tv_modul)).getText()
			// .toString());

			// new s("i: "
			// + i
			// + "  "
			// + ((EditText) v.findViewById(R.id.et_schluessel)).getText()
			// .toString() + "%");
		}

		
	}

	public void onDatenSpeichernClick(View v) {

//		new s(
				new log(this,
						createUpdateQueries()
//						"muh ._."
						);
//				)
		;
//		T.t(v.getContext(), "loool oO ");

	}

	private String[] createUpdateQueries() {

		String[] ret = new String[AlteDatensätze];
		String update = "Update " + DBDaten.T_Noten_Name + " set "
				+ DBDaten.T_Noten_S_Gewichtung + " = ";

		try {
//			modul = verteilung = new String[AlteDatensätze];

			for (int i = 0; i < AlteDatensätze; i++) {

				// getView gibt auch nicht sichtbare elemente aus !
//				View v = view.getAdapter().getView(i, view.getChildAt(i), view);

				// modul[i] = ((TextView)
				// v.findViewById(R.id.tv_modul)).getText()
				// .toString();
				// verteilung[i] = ((EditText)
				// v.findViewById(R.id.et_schluessel))
				// .getText().toString();

				ret[i] = update
				// + ((EditText) v.findViewById(R.id.et_schluessel))
				// .getText().toString()
						+verteilung[i]
				 + " where "
				 + DBDaten.T_Noten_S_Modul
				 + " like '%"+modul[i]
				// + ((TextView) v.findViewById(R.id.tv_modul)).getText()
				// .toString() 
						 + "%'"
				;
				
				new log(this,ret[i]);

			}

		} catch (Exception e) {
			ret[0] = e.toString();
		}

		return ret;

	}

}
