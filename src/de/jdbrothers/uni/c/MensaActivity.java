package de.jdbrothers.uni.c;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.hilfsklassen.ListFiller;
import de.jdbrothers.uni.c.hilfsklassen.PD;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.newMenu;
import de.jdbrothers.uni.c.rss.RSSElement;
import de.jdbrothers.uni.c.rss.RSSReader;

public class MensaActivity extends Activity {

	public static Spinner spin;
	private static ToggleButton tbHeute;
	private static ToggleButton tbMorgen;

	private static String heute;
	private static String morgen;

	static SimpleDateFormat sdf_datum = new SimpleDateFormat("dd.MM.yyyy");

	public static GregorianCalendar cal;

	static ListView view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mensa);

		setTitle(newMenu.mensaA);
		
		view = (ListView) findViewById(R.id.mensalist);
		view.setAdapter(ListFiller.createAdapter(this, R.layout.mensa_item,
				new int[] { R.id.mensaitem_title, R.id.mensaitem_description,
						R.id.mensaitem_price, R.id.mensaitem_info, }));

		cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		sdf_datum.setTimeZone(TimeZone.getTimeZone("GMT"));
		heute = sdf_datum.format(cal.getTime());
		cal.add(Calendar.DAY_OF_YEAR, 1);
		morgen = sdf_datum.format(cal.getTime());

		spin = (Spinner) findViewById(R.id.mensaspinner);
		spin.setOnItemSelectedListener(new MyOnItemSelectedListener()); //

		String[] items = getResources().getStringArray(R.array.mensen_dresden);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, items);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin.setAdapter(adapter);
		datasave.mensaname = spin.getItemAtPosition(0).toString();
		datasave.mensaday = "heute";

		tbHeute = ((ToggleButton) findViewById(R.id.mensa_toggle_heute));
		tbMorgen = ((ToggleButton) findViewById(R.id.mensa_toggle_morgen));
		tbHeute.setChecked(true);

		if (DBAPI.countRows(this, DBDaten.T_Angebot_Name,
				DBDaten.T_Abgebot_S_Datum + " like '%" + heute + "%'") == 0) {
			PD.runIT(this, "Lade Essen");
		} else {
			heute(this, spin);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		sdf_datum.setTimeZone(TimeZone.getTimeZone("GMT"));
		heute = sdf_datum.format(cal.getTime());
		cal.add(Calendar.DAY_OF_YEAR, 1);
		morgen = sdf_datum.format(cal.getTime());

	}

	public static void loadData(Context c, String RSS, String wann) {
		ArrayList<RSSElement> rss = new RSSReader().getFeeds(RSS);
		if (rss != null) {
			String[] headers = { DBDaten.T_Abgebot_S_Mid,
					DBDaten.T_Abgebot_S_Datum, DBDaten.T_Abgebot_S_title,
					DBDaten.T_Abgebot_S_description, DBDaten.T_Abgebot_S_price,
					DBDaten.T_Abgebot_S_URL };
			// System.out.println("Lade aktuelle Daten");
			String[][] angebot = new String[rss.size() + 1][headers.length];
			angebot[0] = headers;

			for (int i = 1; i < rss.size() + 1; i++) {
				RSSElement rsse = rss.get(i - 1);
				angebot[i][0] = rsse.getAuthor();
				angebot[i][1] = "" + wann;
				angebot[i][2] = extractTitle(rsse.getTitle());
				angebot[i][3] = rsse.getFeed();
				angebot[i][4] = extractPrice(rsse.getTitle());
				angebot[i][5] = rsse.getInfo();
			}
			DBAPI.insertInto(c, DBDaten.T_Angebot_Name, angebot);

			System.out.println("Daten aktualisiert");
		} else {
			System.out.println("ging nit...");
		}
	}

	private static String extractPrice(String fromTitle) {
		String s = "";
		ArrayList<Integer> ali = new ArrayList<Integer>();

		for (int i = 0; i < fromTitle.length(); i++) {
			if (fromTitle.charAt(i) == '(' || fromTitle.charAt(i) == ')') {
				ali.add(i);
			}
		}

		if (ali.size() == 4)
			s = fromTitle.substring(ali.get(2) + 1, ali.get(3));
		if (ali.size() == 2)
			s = fromTitle.substring(ali.get(0) + 1, ali.get(1));
		return s;
	}

	private static String extractTitle(String fromTitle) {
		String s = "";

		if (!fromTitle.contains("(")) {
			s = fromTitle;
		} else
			for (int i = 0; i < fromTitle.length(); i++) {
				if (fromTitle.charAt(i) == '(') {
					s = fromTitle.substring(0, i - 1);
				}
			}
		return s;
	}

	public static void fillList(Context c, String datum, String mensaname) {

		String[] selection = { DBDaten.T_Abgebot_S_title,
				DBDaten.T_Abgebot_S_description, DBDaten.T_Abgebot_S_price,
				DBDaten.T_Abgebot_S_URL };

		String[][] tmp = DBAPI.getData(c, DBDaten.T_Angebot_Name, selection,
				DBDaten.T_Abgebot_S_Datum + " = '" + datum + "' AND "
						+ DBDaten.T_Abgebot_S_Mid + " Like '%" + mensaname
						+ "%'");

		if (tmp.length >= 0)
			ListFiller.fill(c, tmp);

		else {
			ListFiller.fill(c, new String[][] { { "Kein Angebot", "",
					"Leider kein Angebot an diesem Tag" } });
		}
	}

	public void onMensaItemClick(View v) {
		datasave.mensaselectedtitle = ((TextView) ((LinearLayout) v)
				.getChildAt(0)).getText().toString();
		datasave.mensaselecteddescription = ((TextView) ((LinearLayout) v)
				.getChildAt(1)).getText().toString();
		datasave.mensaselectedinfo = ((TextView) ((LinearLayout) v)
				.getChildAt(2)).getText().toString();
		if (datasave.mensaselectedinfo != "") {
			startActivity(new Intent(this, Mensa_selectedactivity.class));
		}
	}

	public void onMorgenCLick(View v) {
		morgen(MensaActivity.this, v);
	}

	public void onHeuteClick(View v) {
		heute(MensaActivity.this, v);
	}

	public static void heute(Context c, View v) {
		tbHeute.setChecked(true);
		tbMorgen.setChecked(false);
		datasave.mensaday = "heute";
		fillList(c, heute, datasave.mensaname); // RSS der Mensa abrufen
	}

	public static void morgen(Context c, View v) {
		tbHeute.setChecked(false);
		tbMorgen.setChecked(true);
		datasave.mensaday = "morgen";
		fillList(c, morgen, datasave.mensaname);
	}

	public class MyOnItemSelectedListener implements OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> parent, View v, int pos,
				long row) {
			TextView tv = (TextView)v;
			datasave.mensaname=""+tv.getText();
			
			if(datasave.mensaday=="heute"){
				heute(getApplicationContext(), null);
			}else{
				morgen(getApplicationContext(), null);
			}
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// dann ist mir das wohl egal
		}
	}

	public static void getFood(Context c) {
		DBAPI.delete(c, DBDaten.T_Angebot_Name);
		loadData(
				c,
				"http://www.studentenwerk-dresden.de/feeds/speiseplan.rss?tag=heute",
				heute);
		System.out.println("heute gespeichert, lade morgen.");
		loadData(
				c,
				"http://www.studentenwerk-dresden.de/feeds/speiseplan.rss?tag=morgen",
				morgen);
	}

	public static void handleIT(Context c) {
		// Methode f�r PD
		heute(c, spin);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(newMenu.createMenu(menu, new String[] {
				newMenu.neuLaden, newMenu.hauptmen� }));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(newMenu.getAction(item, this));
	}

}
