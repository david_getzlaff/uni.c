package de.jdbrothers.uni.c;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.TabActivity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.db.DBManager;
import de.jdbrothers.uni.c.hilfsklassen.PD;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.dateWorker;
import de.jdbrothers.uni.c.hilfsklassen.newMenu;
import de.jdbrothers.uni.c.hilfsklassen.planImporter;

public class PlanActivity extends TabActivity {

	private static ListView lv;
	private static TabHost host;
	private static Context c;
	static ArrayList<HashMap<String, String>> myListing;

	private static TextView setTag, lastUpdate;

	private static SimpleAdapter sa;

	private static String datenHerunterladen = "Bitte Daten herunterladen";

	public static Spinner sp;
	

	static String[] bez = { "Von", "Bis", "Modul", "Ort" };
	static int[] res = { R.id.tv_item_von, R.id.tv_item_bis,
			R.id.tv_item_Modul, R.id.tv_item_Ort };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Layout darstellen; DB, Elemente initialisieren
		setContentView(R.layout.plan_host_layout);
		setTitle(newMenu.planA);

		c = this;
		SQLiteDatabase db = new DBManager(this).getReadableDatabase();
		new DBManager(this).onCreate(db);
		db.close();

		myListing = new ArrayList<HashMap<String, String>>();
		lv = (ListView) findViewById(R.id.lv_plan);
		sp = (Spinner) findViewById(R.id.sp_chooseDates);

		host = getTabHost();
		setTag = ((TextView) findViewById(R.id.tv_setTag));
		lastUpdate= ((TextView) findViewById(R.id.tv_stand_update));

		{
			// Setze Tabs
			String setTag = "";
			for (int i = 0; i < 6; i++) {
				setTag = dateWorker.getDayOfTheWeek(i + 2);
				host.addTab(host.newTabSpec(setTag).setIndicator(setTag)
						.setContent(R.id.layout_main));
			}

			host.setOnTabChangedListener(new OnTabChangeListener() {
				@Override
				public void onTabChanged(String tabId) {
					setDate(tabId);
					checkEntries(tabId);
				}
			});
		}

		sp.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View v, int pos,
					long row) {
				checkEntries(host.getCurrentTabTag());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		fillSpinner(this);
		setSA();
		host.setCurrentTab(1);
		host.setCurrentTabByTag(dateWorker.getDayOfTheWeek());

		setDate(host.getCurrentTabTag());
		showLastUpdate();
	}

	private static void setDate(String tag) {
		setTag.setText(tag + " - "
				+ (dateWorker.getDateOfDay(host.getCurrentTab(), sp)));
	}

	private static void showLastUpdate() {
		// Liest letztes Update aus DB aus und setzt es auf tv_stand_update
		String s = DBAPI.getData(c, DBDaten.T_lastUpdate_Name,
				DBDaten.T_lastUpdate_S_Wann, "where was = 'plan_woche_"
						+ sp.getSelectedItem().toString() + "'")[0][0];
		if (s.length() > 0) {
			lastUpdate.setText("Stand: " + s);
			lastUpdate.setVisibility(View.VISIBLE);
		} else {
			lastUpdate.setVisibility(View.INVISIBLE);
	}}

	public void onItemClick(View v) {
		String s = ((TextView) ((LinearLayout) ((LinearLayout) ((LinearLayout) v)
				.getChildAt(0)).getChildAt(1)).getChildAt(0)).getText()
				.toString();
		if (s.equals(datenHerunterladen)) {
			PD.runIT(this, "Lade Stundenplan");
		}
	}

	private void checkEntries(String wochentag) {
		if (DBAPI.getData(c, DBDaten.T_Plan_Name, new String[] { "Von, Bis, "
				+ wochentag }, " where Datum = '"
				+ sp.getSelectedItem().toString() + "'").length < 1) {
			empfiehlDownload(c, sp.getSelectedItem().toString());
		} else
			showPlan(wochentag);
	}

	private static void empfiehlDownload(Context c, String string) {
		HashMap<String, String> item = new HashMap<String, String>();

		if (!setTag.getText().toString()
				.contains("-- Keine Daten vorhanden --"))
			setTag.setText(setTag.getText() + "\n-- Keine Daten vorhanden --");
		item.put(bez[2], datenHerunterladen); // setzt das in "Modul"

		myListing.add(new HashMap<String, String>());
		myListing.add(item);

		item = new HashMap<String, String>();

		setSA();
	}

	public static void ladeDaten(final Context c, final String woche) {
		try {
			String week= "plan_woche_"+sp.getSelectedItem().toString();
			if(planImporter.importIT(c, woche) ==true)
				//Update war erfolgreich
			{
				DBAPI.delete(c, DBDaten.T_lastUpdate_Name, "where was = '"+week+"'");
				System.out.println("Import war erfolgreich; insert: "+week);
				DBAPI.aktualisiereUpdateTable(c, week);
				showLastUpdate();
			}
			else{
				System.out.println("Import war nicht erfolgreich");
				DBAPI.delete(c, DBDaten.T_lastUpdate_Name, "where was = '"+week+"'");
			}
		} catch (Exception e) {
			T.t(c, "Fehler beim Laden");
			e.printStackTrace();
		}
	}

	public void fillSpinner(Context c) {
		ArrayAdapter<String> aa = new ArrayAdapter<String>(c,
				android.R.layout.simple_spinner_item, dateWorker.buildWeek());
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp.setAdapter(aa);
	}

	public static void showPlan(String tabId) {
		//baut listview auf 

		HashMap<String, String> item = new HashMap<String, String>();
		String[][] values = new String[0][0];
		values = DBAPI.getData(c, DBDaten.T_Plan_Name, new String[] { "Von",
				"Bis", tabId },
				"" + " where Datum = '" + (sp.getSelectedItem().toString())
						+ "'");
		String raum = "";
		String modul = "";

		if (values.length > 0) {
			for (int i = 0; i < values.length; i++) {
				{
					if (values[i][2].length() > 4 || values[i][2].contains("."))
						try {

							char[] tmp = values[i][2].toCharArray();
							int pkt = 1;
							{
								int dotCounter = 0;

								for (int findDot = 0; findDot < tmp.length; findDot++) {
									if (tmp[findDot] == '.') {
										pkt = findDot;
										dotCounter++;
									}
								}
								if (dotCounter < 2) {
									raum = new String(tmp).substring(pkt - 1,
											pkt + 4);

									// System.out.println(raum);

									modul = new String(tmp).substring(0,
											pkt - 1)
											+ new String(tmp).substring(
													pkt + 4, tmp.length);
								} else {
									modul = values[i][2];
									raum = "";
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					else {
						modul = values[i][2];
						raum = "";
					}

					item.put(bez[0], values[i][0]);
					item.put(bez[1], values[i][1]);
					item.put(bez[2], modul);
					item.put(bez[3], raum);
				}
				myListing.add(item);
				item = new HashMap<String, String>();
			}
		} else {
			empfiehlDownload(c, sp.getSelectedItem().toString());
		}
		setSA();
	}

	private static void setSA() {
		sa = new SimpleAdapter(c, myListing, R.layout.plan_host_layout_item,
				bez, res);
		lv.setAdapter(sa);
		sa.notifyDataSetChanged();
		myListing = new ArrayList<HashMap<String, String>>();
	}

	public void onExitClick(View v) {
		finish();
	}

	public void onDatenLöschenClick(View v) {
		DBAPI.delete(c, DBDaten.T_Plan_Name);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(newMenu.createMenu(menu, new String[] {
				newMenu.planFürWocheAktualiesieren, newMenu.allesLöschen }));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(newMenu.getAction(item, this));
	}
}