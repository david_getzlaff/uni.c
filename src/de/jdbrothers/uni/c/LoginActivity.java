package de.jdbrothers.uni.c;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.hilfsklassen.AD;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.newMenu;

public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		setTitle(newMenu.loginA);
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Logindaten
		datasave.refresh(this);
		String[] data = DBAPI.getLoginData(this);
		datasave.Nummer_Alt = data[0];
		((EditText) findViewById(R.id.et_MatrNr)).setText(data[0]);
		((EditText) findViewById(R.id.et_PW)).setText(data[1]);
		((Spinner) findViewById(R.id.timeoutspinner)).setSelection(Integer
				.parseInt(data[3]));

		// Schriftgr��e
		Spinner sp_haupttext = (Spinner) findViewById(R.id.sp_haupttext);
		Spinner sp_nebentext = (Spinner) findViewById(R.id.sp_nebentext);

		List<String> schriftgr��en = new ArrayList<String>();
		for (int i = 10; i < 50; i = i + 2) {
			schriftgr��en.add(i + "dp");
		}

		ArrayAdapter<String> textSizes = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, schriftgr��en);
		textSizes
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp_haupttext.setAdapter(textSizes);
		sp_nebentext.setAdapter(textSizes);

	}

	public void DatenSpeichern(View v) {
		String[] Input = { "MatrNr", "Passwort", "TimeOut", "Position" };
		Input[0] = ((EditText) findViewById(R.id.et_MatrNr)).getText()
				.toString();
		Input[1] = ((EditText) findViewById(R.id.et_PW)).getText().toString();
		Input[2] = ((((Spinner) findViewById(R.id.timeoutspinner))
				.getSelectedItem().toString()));
		Input[3] = String
				.valueOf((((Spinner) findViewById(R.id.timeoutspinner))
						.getSelectedItemPosition()));

		if // Leere Eintr�ge nicht gew�nscht !
		(Input[0].length() < 3 || Input[1].equals("")) {
			T.t(this, "Bitte Daten �berpr�fen!");

		} else

		if // DB ist leer -->Speichere Daten
		(DBAPI.Check4Entries(this)) {
			DBAPI.setLoginData(this, Input);
		} else {
			// Bei Matrnr-�nderung muss die DB gel�scht werden
			if (!Input[0].equals(datasave.Nummer_Alt)) {
				AD.ring(this, AD.userHasChanged,
						"User hat sich ge�ndert. Alle Daten werden gel�scht !",
						DBDaten.T_Settings_Name, Input);
			} else {
				AD.ring(this, AD.updateTable, "Logindaten aktualisieren?",
						DBDaten.T_Settings_Name, Input);
			}
		}
		datasave.cookie = null;
	}

	public void onResetClick(View v) {
		startActivity(new Intent(this, ResetMenueActivity.class));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(newMenu.createMenu(menu,
				new String[] { newMenu.hauptmen� }));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(newMenu.getAction(item, this));
	}

}
