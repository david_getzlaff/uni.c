package de.jdbrothers.uni.c;

import java.io.IOException;
import java.util.Iterator;

import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import de.jdbrothers.uni.c.hilfsklassen.datasave;

public class Mensa_selectedactivity extends Activity {
 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mensa_selected_item);	
		  datasave.TempContext=this;
		((TextView)findViewById(R.id.mensa_selected_item_title)).setText(datasave.mensaselectedtitle);
		((TextView)findViewById(R.id.mensa_selected_item_description)).setText(datasave.mensaselecteddescription);
	//	((WebView)findViewById(R.id.mensa_selected_item_webview)).loadUrl(datasave.mensaselectedinfo);
		WebView view =((WebView)findViewById(R.id.mensa_selected_item_webview));
		addFoodPicture(view);
		
		
	}	
	
	private void addFoodPicture(WebView view){
		try {
			
			Document document;
			
			org.jsoup.Connection.Response response = Jsoup.connect(datasave.mensaselectedinfo)
					.userAgent("Mozilla")
					.timeout(datasave.timeout)					
					.method(Method.POST).execute();
			
			document = Jsoup.parse(response.body());
			
			Iterator<Element> ite = document.select("html body div#main div#spalterechtsnebenmenue div div").iterator();
			ite.next(); //kopfzeile überspringen
			String html = "<table>" + ite.next().html() + "</table>";
			String css = "<style type='text/css'><!--#textkleinrot {	font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 8px;	color:#FF0000;}#textkleinschwarz {	font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;	color:#000000;}#textsehrkleinschwarz {	font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 8px;	color: #000000;}	td {border-width: 1px;	border-spacing: 2px;	border-style: outset;	border-color: gray;	border-collapse: collapse;	}	html body table tbody tr td table tbody tr td{	border: none;	}--></style>";
			String www = "<html>" + css + "<body>" + html + "</body></html>";
			 view.loadDataWithBaseURL("fake://fake.example", www, "text/html", "UTF-8", null);				
			view.invalidate();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void addFoodInformation(WebView view){
		try {
			
			Document document;
			
			org.jsoup.Connection.Response response = Jsoup.connect(datasave.mensaselectedinfo)
					.userAgent("Mozilla")
					.timeout(datasave.timeout)					
					.method(Method.POST).execute();
			
			document = Jsoup.parse(response.body());
			
			Iterator<Element> ite = document.select("html body div#main div#spalterechtsnebenmenue div div ").iterator();
			ite.next(); //kopfzeile überspringen
			String html = "<table>" + ite.next().html() + "</table>";
			String css = "<style type='text/css'><!--#textkleinrot {	font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 8px;	color:#FF0000;}#textkleinschwarz {	font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 9px;	color:#000000;}#textsehrkleinschwarz {	font-family: Verdana, Arial, Helvetica, sans-serif;	font-size: 8px;	color: #000000;}	td {border-width: 1px;	border-spacing: 2px;	border-style: outset;	border-color: gray;	border-collapse: collapse;	}	html body table tbody tr td table tbody tr td{	border: none;	}--></style>";
			String www = "<html>" + css + "<body>" + html + "</body></html>";
			 view.loadDataWithBaseURL("fake://fake.example", www, "text/html", "UTF-8", null);	
			
			
			
			view.invalidate();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}