package de.jdbrothers.uni.c;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.db.DBManager;
import de.jdbrothers.uni.c.hilfsklassen.AD;
import de.jdbrothers.uni.c.hilfsklassen.Checker;
import de.jdbrothers.uni.c.hilfsklassen.ListFiller;
import de.jdbrothers.uni.c.hilfsklassen.PD;
import de.jdbrothers.uni.c.hilfsklassen.Spyder;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.imageHelper;
import de.jdbrothers.uni.c.hilfsklassen.newMenu;

public class ContactActivity extends Activity  {

	public static String loginurl = "http://www.ba-dresden.de/index2.php?menu=5";
	public static String dataurl = "http://www.ba-dresden.de/index2.php?menu=5&item=21";
	public static String errorExpression = "html body div#world div#Page2 table tbody tr td div#PageContent2 div#PageSubContent2 h6";
	public static String errorResult = "Sie m�ssen sich erst anmelden!";

	ListView view;

//	public static ProgressDialog pd;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact);

		setTitle(newMenu.contactA);

		datasave.TempContext = this;
		view = (ListView) findViewById(R.id.listViewContact);

		view.setAdapter(ListFiller.createAdapter(this, R.layout.contact_item,
				new int[] { R.id.Titel, R.id.Name, R.id.Email, R.id.Phone,
						R.id.Adress }));

		if (DBAPI.countRows(this, DBDaten.T_Kontakt_Name) == 0) {
			PD.runIT(this, "Lade Kontakte");

		} else {
			listContacts(this);
		}
	}

	public static void listContacts(Context c) {
		ListFiller.fill(c, getTheseData(c));
	}

	private static String[][] getTheseData(Context c) {
		return DBAPI.getData(c, DBDaten.T_Kontakt_Name, new String[] {
				DBDaten.T_Kontakt_S_Titel, DBDaten.T_Kontakt_S_Name,
				DBDaten.T_Kontakt_S_Email, DBDaten.T_Kontakt_S_Telefonnr,
				DBDaten.T_Kontakt_S_Adresse }, "");
	}

	public static void KontakteHerunterladenUndSpeichern(Context c) {
		Iterator<org.jsoup.nodes.Element> ite;
		Iterator<org.jsoup.nodes.Element> ite2;
		Iterator<Element> ite_k = null;
		String myImageUrl = "";
		if (Checker.checkBetaDate(c)) {
			T.s("NotenActivity", "Beta l�uft noch");
		try {
			Document document = new Spyder()
					.getDoc(loginurl, dataurl, datasave.getConkeys(),
							datasave.getCondata(c), datasave.timeout,
							Method.POST, errorExpression, errorResult);
			ite_k = document
					.select("html body div#world div#Page2 table tbody tr td div#PageContent2 div#PageSubContent2 table tbody tr td#ansprech")
					.iterator();

		} catch (Exception e) {
			T.t(c, "Fehler beim Verbinden - Bitte Daten pr�fen");
		}

		if ((ite_k != null)) {
			ContentValues Kontakte = new ContentValues();

			ArrayList<ContentValues> bob = new ArrayList<ContentValues>();

			try {
				while (ite_k.hasNext()) {
					Document k_document = Jsoup.parse(ite_k.next().html());
					// =============================================
					// =============================================
					try {

						String s = k_document.select("tbody tr td img[src]")
								.attr("src");// Bildquelle auslesen
						char[] chars = new char[s.length()];
						s.getChars(2, s.length(), chars, 0);// .. vor quelle
															// entfernen
						myImageUrl = "http://www.ba-dresden.de"
								+ new String(chars); // Quelle zusammensetzen

					} catch (Exception e) {
						T.t(c, "Bilder konnten nicht gefunden werden");
					}

					// =============================================
					// =============================================

					Kontakte.put("Titel", k_document.select("tbody tr td h5")
							.text());
					ite2 = k_document.select("tbody tr td").iterator();
					ite2.next();
					Kontakte.put("Name", ite2.next().ownText());
					ite = k_document.select("tbody tr td table tbody tr td")
							.iterator();
					ite.next();
					Kontakte.put("Adress", ite.next().text());
					ite.next();
					Kontakte.put("Phone", ite.next().text());
					ite.next();
					ite.next(); // Fax
					ite.next();
					Kontakte.put("Email", ite.next().text());

					// Bilder runter laden
					Kontakte.put(DBDaten.T_Kontakt_S_Image, imageHelper
							.saveImage(new URL(myImageUrl)).toByteArray());

					// ContentValue in DB speichern
					bob.add(Kontakte);
					Kontakte = new ContentValues();
				}// While Ende
				if (bob.size() > 1) {
					DBAPI.delete(c, DBDaten.T_Kontakt_Name);
					SQLiteDatabase db = new DBManager(c).getReadableDatabase();
					for (int i = 0; i < bob.size(); i++) {
						db.insert(DBDaten.T_Kontakt_Name, null, bob.get(i));
					}
					db.close();
//					T.t(c, "Kontakte aktualisiert");
				}
				else {
					T.t(c, "Keine Daten zum Speichern gefunden");
				}
			} catch (Exception e) {
				datasave.cookieerror(true);
				T.t(c, "Fehler beim Verbinden - Bitte Daten pr�fen");
			}
		}} else {
			AD.showAlert(c, "Beta Abgelaufen");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(newMenu.createMenu(menu, new String[] {
				newMenu.neuLaden, newMenu.hauptmen� }));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(newMenu.getAction(item, this));
	}

	public void onContactClick(View v) {
		Intent i = new Intent(this, Contact_selectedactivity.class);
		datasave.contactselectedtelefon = ((TextView) ((LinearLayout) v)
				.getChildAt(3)).getText().toString();
		datasave.contactselectedemail = ((TextView) ((LinearLayout) v)
				.getChildAt(2)).getText().toString();
		datasave.contactselectedaddress = ((TextView) ((LinearLayout) v)
				.getChildAt(4)).getText().toString();
		datasave.contactselectedname = ((TextView) ((LinearLayout) v)
				.getChildAt(1)).getText().toString();

		startActivity(i);
	}
//
//	@Override
//	public void run() {
//
//		Looper.prepare();
//
//		KontakteHerunterladenUndSpeichern(ContactActivity.this);
//		System.out.println("Daten heruntergeladen");
//		myHandler.sendEmptyMessage(0);
//
//		Looper.loop();
//	}
//
//	private Handler myHandler = new Handler() {
//
//		@Override
//		public void handleMessage(Message msg) {
//
//			pd.dismiss();
//			listContacts(ContactActivity.this);
//			super.handleMessage(msg);
//		}
//
//	};
}