package de.jdbrothers.uni.c.rss;

public class RSSElement {
	private String title;
	private String info;
	private String feed;
	private String author;

	public RSSElement() {

	}

	public RSSElement(String title, String info, String feed) {
		parse(title, info, feed, null);
	}

	public RSSElement(String title, String info, String feed, String author) {
		parse(title, info, feed, author);
	}

	public RSSElement(String title, String feed) {
		parse(title, null, feed, null);
	}

	public RSSElement(String feed) {
		parse(null, null, feed, null);
	}

	private void parse(String title, String info, String feed, String author) {
		this.title = title;
		this.info = info;
		this.feed = feed;
		this.author = author;

	}

	public String getTitle() {
		return title;
	}

	public String getInfo() {
		return info;
	}

	public String getFeed() {
		return feed;
	}

	public String getAuthor() {
		return author;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

}
