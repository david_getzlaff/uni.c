package de.jdbrothers.uni.c.rss;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.datasave;

public class RSSReader {
	private String readline = "";
	private ArrayList<RSSElement> elements = new ArrayList<RSSElement>();
	private String link;

	public ArrayList<RSSElement> getFeeds(String url) {
		return getFeeds(url, "");
	}

	public ArrayList<RSSElement> getFeeds(String url, String filter) {
		String title = "";
		String feed = "";
		String author = "";
		try {
			URL feedURL = new URL(url);
			BufferedReader buff = new BufferedReader(new InputStreamReader(
					feedURL.openStream(), "iso-8859-1"));

			while ((readline = buff.readLine()) != null) {

				if (readline.contains("<title>")) {
					String buffstring = readline.substring(7,
							readline.length() - 8);
					title = buffstring;
				}

				if (readline.contains("<description>")) {
					String buffstring = readline.substring(13,
							readline.length() - 14);
					feed = buffstring;
				}
				if (readline.contains("<link>")) {
					String buffstring = readline.substring(6,
							readline.length() - 7);
					link = buffstring;
				}
				if (readline.contains("<author>")) {
					String buffstring = readline.substring(8,
							readline.length() - 9);
					author = buffstring;

				}

				if (title != "" && feed != "" && link != ""
						&& author.contains(filter) && author!="") {
					title = title.replace("(", "\n(");
					elements.add(new RSSElement(title, link, feed, author));
					title = "";
					feed = "";
					link = "";
					author = "";
				}

			}

			return elements;

		} catch (Exception e) {
			T.t(datasave.TempContext, "Fehler beim Laden des RSS-Feeds");
			e.printStackTrace();
			return null;
		}

	}
}