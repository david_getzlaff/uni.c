package de.jdbrothers.uni.c;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.db.DBManager;
import de.jdbrothers.uni.c.db.s;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.newMenu;

public class _mainActivity extends Activity
// implements Runnable
{

	public static boolean �bergabe;

	public static ProgressDialog pd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		this.setTitle(newMenu.mainA);

		// legt alle Tabellen an, if not exist
		SQLiteDatabase db = new DBManager(this).getReadableDatabase();
		new DBManager(this).onUpgrade(db, DBManager.Erstinitialisierung);
		// System.out.println(db.getPath());
		// /data/data/de.jdbrothers.uni.c/databases/Datenbank.db
		db.close();

		�bergabe = false;

		checkForLoginData(this);
		checkFornNewNotenTable(this);
		datasave.TempContext = this;
		datasave.refresh(this);

	}

	private void checkForLoginData(Context c) {
		if (DBAPI.getLoginData(c)[0].equals("000")
				&& DBAPI.getLoginData(c)[1].equals(""))
			startActivity(new Intent(c, LoginActivity.class));
	}

	private void checkFornNewNotenTable(Context c) {
		/*
		 * Liest aktuelle tabelle Noten aus. wenn die Tabelle nicht die spalte
		 * "Gewichtung" enthalt, wird diese dementsprechend aktualisiert
		 */

		String[] tmp = DBAPI.getHeaders(c, DBDaten.T_Noten_Name);
		boolean neueNotenTabelle = false;
		for (int i = 0; i < tmp.length; i++)
			if (tmp[i].equals(DBDaten.T_Noten_S_Gewichtung)) {
				neueNotenTabelle = true;
			}

		if (!neueNotenTabelle) {
			new s("Tabelle Noten wird aktualisiert");
			T.t(c, "Tabelle \"Noten\" wird �berarbeitet");
			SQLiteDatabase db = new DBManager(c).getReadableDatabase();
			new DBManager(c).onUpgrade(db, DBManager.updateNoten);
			db.close();
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (�bergabe == true)
			System.exit(0);
		DBAPI.setFilterpos(this, 0);
		datasave.refresh(this);
	}

	public void onNoteClick(View v) {
		// startActivity(new Intent(this, NotenActivity.class));
		startActivity(new Intent(this, NotenschluesselActivity.class));
	}

	public void onNewsClick(View v) {
		startActivity(new Intent(this, NewsActivity.class));
	}

	public void onPlanClick(View v) {
		startActivity(new Intent(this, PlanActivity.class));
	}

	public void onContactClick(View v) {
		startActivity(new Intent(this, ContactActivity.class));
	}

	public void onMensaClick(View v) {
		startActivity(new Intent(this, MensaActivity.class));
	}

	public void onLoginClick(View v) {
		startActivity(new Intent(this, LoginActivity.class));
	}

	// public void onOKCLick(View v) {
	// et_entwickler_pw = (EditText) findViewById(R.id.et_entwickler_pw);
	// String PW = et_entwickler_pw.getText().toString();
	//
	// if (PW.equals("bob1337")) {
	// // Speichere in DB-Settings
	// SQLiteDatabase db = new DBManager(this).getReadableDatabase();
	// try {
	//
	// db.execSQL(DBAPI.createInsertQuery(DBDaten.T_Settings_Name,
	// new String[] { DBDaten.T_Settings_S_Attribut,
	// DBDaten.T_Settings_S_Wert }, new String[] {
	// "AdminPW", PW }));
	// } catch (Exception e) {
	//
	// System.out.println("-------Problem beim Inserten------");
	// e.printStackTrace();
	// System.out.println("-------Problem beim Inserten Ende------");
	//
	// }
	//
	// db.close();
	// T.t(this, "Hallo, Entwickler :)");
	// setContentView(R.layout.main);
	// }
	//
	// }

	// public boolean check4Admin(Context c) {
	//
	// boolean tmp = false;
	// // Lies PW aus DB aus; tmp = true;
	// SQLiteDatabase db = new DBManager(c).getReadableDatabase();
	// try {
	// Cursor cursor = db.rawQuery("Select Wert From Settings Where "
	// + DBDaten.T_Settings_S_Attribut + " like 'AdminPW'", null);
	//
	// cursor.moveToNext();
	// startManagingCursor(cursor);
	//
	// if (cursor.getCount() == 0)
	// ;
	// else
	//
	// if (cursor.getString(0).equals("bob1337")) {
	// tmp = true;
	// }
	// cursor.close();
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// db.close();
	//
	// return tmp;
	// }

	public void onExitClick(View v) {
		System.exit(0);
	}

	// @Override
	// public void run() {
	//
	// Looper.prepare();
	// int m = 0;
	// if (true) {
	// m = 0;
	//
	// }
	// myHandler.sendEmptyMessage(m);
	// Looper.loop();
	//
	// }
	//
	// Handler myHandler = new Handler() {
	//
	// @Override
	// public void handleMessage(Message msg) {
	//
	// pd.dismiss();
	// String s = (msg.toString()).substring(7, 8);
	// if (Integer.parseInt(s) == 0) {
	// setContentView(R.layout.main);
	//
	// if (DBAPI.
	// // DBLoginAuslesen
	// getLoginData
	// (_mainActivity.this)[0].equals("000")) {
	// startActivity(new Intent(_mainActivity.this,
	// LoginActivity.class));
	// }
	// } else
	// setContentView(R.layout.beta_ende);
	// super.handleMessage(msg);
	// }
	//
	// };

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(newMenu.createMenu(menu,
				new String[] { newMenu.resetmen� }));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(newMenu.getAction(item, this));
	}

}
