package de.jdbrothers.uni.c;

import java.util.ArrayList;
import java.util.Iterator;
import org.jsoup.Connection.Method;
import org.jsoup.nodes.Document;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import de.jdbrothers.uni.c.db.DBAPI;
import de.jdbrothers.uni.c.db.DBDaten;
import de.jdbrothers.uni.c.db.DBManager;
import de.jdbrothers.uni.c.hilfsklassen.AD;
import de.jdbrothers.uni.c.hilfsklassen.Checker;
import de.jdbrothers.uni.c.hilfsklassen.ListFiller;
import de.jdbrothers.uni.c.hilfsklassen.PD;
import de.jdbrothers.uni.c.hilfsklassen.Spyder;
import de.jdbrothers.uni.c.hilfsklassen.T;
import de.jdbrothers.uni.c.hilfsklassen.datasave;
import de.jdbrothers.uni.c.hilfsklassen.newMenu;

public class NotenActivity extends Activity {

	public static String loginurl = "http://www.ba-dresden.de/index2.php?menu=5";
	public static String dataurl = "http://www.ba-dresden.de/index2.php?menu=5&item=23";
	public static String errorExpression = "html body div#world div#Page2 table tbody tr td div#PageContent2 div#PageSubContent2 h6";
	public static String errorResult = "Sie m�ssen sich erst anmelden!";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.noten);

		setTitle(newMenu.notenA);

		ListView view = (ListView) findViewById(R.id.listViewNoten);
		view.setAdapter(ListFiller
				.createAdapter(this, R.layout.note_item, new int[] {
						R.id.textSubject, R.id.textSemester, R.id.textMark }));




		int AlteDatens�tze = DBAPI.countRows(this, DBDaten.T_Noten_Name);

		if (DBAPI.countRows(this, DBDaten.T_Noten_Name) == 0) {
			PD.runIT(this, "Lade Noten");

		} else {
			filter(this, "" + DBAPI.getFilterpos(this));
		}

		pr�feAufNeueDaten(AlteDatens�tze);

		// Notendurchschnitt setzen
		try {
			String note = DBAPI.ViewDurchschnitt(this);
			
			((TextView) findViewById(R.id.durchschnittsNote)).setText((note
					.equals("") ? "0.00" : note));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void filter(Context c, String zahl) {
		if (zahl.equals("0"))
			zahl = "%";
		ListFiller.fill(
				c,
				getTheseData(c, " where " + DBDaten.T_Noten_S_Semester
						+ " like '%" + zahl + "%'"));
	}

	private static String[][] getTheseData(Context c, String where) {
		return DBAPI.getData(c, DBDaten.T_Noten_Name, new String[] {
				DBDaten.T_Noten_S_Modul, DBDaten.T_Noten_S_Semester,
				DBDaten.T_Noten_S_Note }, where);
	}

	public static void NotenHerunterladenUndSpeichern(Context c) {
		Iterator<org.jsoup.nodes.Element> ite = null;
		if (Checker.checkBetaDate(c)) {
			T.s("NotenActivity", "Beta l�uft noch");
			try {

				Document document = new Spyder().getDoc(loginurl, dataurl,
						datasave.conkeys, datasave.getCondata(c),
						datasave.timeout, Method.POST, errorExpression,
						errorResult);

				((org.jsoup.nodes.Element) document)
						.select("html body div#world div#Page2 table tbody tr td div#PageContent2 div#PageSubContent2 div#std_inh table tbody tr td table");

				ite = ((org.jsoup.nodes.Element) document)
						.select("html body div#world div#Page2 table tbody tr td div#PageContent2 div#PageSubContent2 div#std_inh table tbody tr td table tbody tr td")
						.iterator();
				ite.next();
				ite.next();
				ite.next();
				ite.next();
				ite.next();
				ite.next();
				ite.next();
			} catch (Exception e) {
				T.t(c, "Fehler beim Verbinden - Bitte Daten pr�fen");
				T.e("NotenActivity", e);
			}

			if (ite != null) {
				Integer n = 0;
				ContentValues Noten = new ContentValues();

				ArrayList<ContentValues> bob = new ArrayList<ContentValues>();

				try {
					while (ite.hasNext()) { // Zeilenweise auslesen
						if (n == 0) { // Ganz cleverer Aufbau - hat Julian gut
										// gebaut ;)
							String Modul = ite.next().text();
							Noten.put(DBDaten.T_Noten_S_Modul, Modul);
						} else {
							String s = ite.next().text();
							if (s.length() >= 5) {
								Noten.put(DBDaten.T_Noten_S_Semester,
										(n.toString() + ". Semester"));
								Noten.put(DBDaten.T_Noten_S_Note,
										s.substring(1, s.length() - 1));
							}
						}
						if (n == 6) {
							bob.add(Noten);
							Noten = new ContentValues();
							n = 0;
						} else {
							n++;
						}
					}

					if (bob.size() > 1) {
						DBAPI.delete(c, DBDaten.T_Noten_Name);
						SQLiteDatabase db = new DBManager(c)
								.getReadableDatabase();
						for (int i = 0; i < bob.size(); i++) {
							db.insert(DBDaten.T_Noten_Name, null, bob.get(i));
						}
						db.close();
					} else {
						T.t(c, "Keine Daten zum Speichern gefunden");
					}

				} catch (Exception e) {
					datasave.cookieerror(true);
				}
			}
		} else {
			AD.showAlert(c, "Beta Abgelaufen");
		}
	}

	public void pr�feAufNeueDaten(int alteDatens�tze) {
		if (DBAPI.countRows(this, DBDaten.T_Noten_Name) > alteDatens�tze) {
			T.t(this, "Yo, neue Noten :D");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(newMenu.createMenu(menu, new String[] {
				newMenu.neuLaden, newMenu.filter, newMenu.notenschl�ssel }));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(newMenu.getAction(item, this));
	}

	public void onClick(View v) {
		String shareText = "Ich habe im ";
		LinearLayout main = (LinearLayout) v;

		LinearLayout left = (LinearLayout) main.getChildAt(0);
		TextView modul = (TextView) left.getChildAt(0);
		TextView semester = (TextView) left.getChildAt(1);
		TextView note = (TextView) main.getChildAt(1);

		shareText = shareText + semester.getText() + "\n" + "Im Modul \""
				+ modul.getText() + "\"\n" + "die Note "
				+ note.getText().subSequence(0, 3) + " erreicht.";
		// System.out.println(shareText);
		T.s(this, shareText);

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("text/plain");
		i.putExtra(Intent.EXTRA_TEXT, shareText);
		startActivity(Intent.createChooser(i, "Share"));
	}
}
